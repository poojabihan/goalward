//
//  CustomeView.m
//  GifDemo
//
//  Created by Alok Mishra on 08/12/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "CustomeView.h"

@implementation CustomeView



- (void)drawRect:(CGRect)rect {
    
    self.ratingView.backgroundColor = [UIColor clearColor];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"goli-2" withExtension:@"gif"];
    self.Gotimage.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    self.ratingView = [[RatingView alloc] initWithFrame:CGRectMake(0, 2.5, 200, 45)
                                      selectedImageName:@"selected.png"
                                        unSelectedImage:@"unSelected.png"
                                               minValue:0
                                               maxValue:5
                                          intervalValue:0.5
                                             stepByStep:NO];
    self.ratingView.delegate = self;
    [self.RatView addSubview:self.ratingView];
    self.ratingView.value=1.0;
}
- (void)rateChanged:(RatingView *)sender{
    
    RatingValue = self.ratingView.value;

    NSUserDefaults *store=[NSUserDefaults standardUserDefaults];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",self.ratingView.value] forKey:@"Rating"];
    
    NSLog(@"Store Value Check %@",[store valueForKey:@"Rating"]);
    
    NSLog(@"%f  -   -   %@",self.ratingView.value,self.RatingString);
}

@end
