//
//  TaskListViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskListViewController : UIViewController


@property (strong, nonatomic) IBOutlet UILabel *TaskName;
@property (strong, nonatomic) IBOutlet UILabel *TotalGolies;

@property (strong, nonatomic) IBOutlet UITableView *Tableview;
@property (strong, nonatomic) NSString *EventID;
@end
