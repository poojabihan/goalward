//
//  CommentGuidlineTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 27/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentGuidlineTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *GuidlienText;


@end
