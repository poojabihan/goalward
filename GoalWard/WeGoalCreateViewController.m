//
//  WeGoalCreateViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "WeGoalCreateViewController.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "WeGuideLineCellTableViewCell.h"
#import "WeGoalPeopleViewController.h"
#import <FCAlertView.h>
@interface WeGoalCreateViewController ()<FCAlertViewDelegate>
{
    NSMutableArray *TitleData;NSInteger TagCount;UIDatePicker *datePicker;NSString *dateString;
    MBProgressHUD *hud;NSDate *eventDateStart;NSDate *eventDateEnd;
    NSString *stratDateString;UIColor *color;NSString *subtitleColor;
    NSString *endDateString;NSMutableArray *selectedCandidateRolesArray;
}
@property (retain, nonatomic) UIImage *alertImage;
@property (retain, nonatomic) NSString *alertTitle;
@property (retain, nonatomic) NSArray *arrayOfButtonTitles;
@property NSDateFormatter *dateFormat;
@property NSDateFormatter *SecDateformat;
@end
@implementation WeGoalCreateViewController
- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self) {
        self.SecDateformat = [[NSDateFormatter alloc] init];
        [self.SecDateformat setDateFormat:@"dd/MM/YYYY"];
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    _ScrollContainView.backgroundColor=[UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    selectedCandidateRolesArray = [[NSMutableArray alloc] init];
    TitleData = [[NSMutableArray alloc] init];
    _Tableview.scrollEnabled=YES;
    _Tableview.backgroundColor=[UIColor clearColor];
    _Tableview.estimatedRowHeight=_Tableview.rowHeight;
    _Tableview.rowHeight=UITableViewAutomaticDimension;
   // [selectedCandidateRolesArray  addObject:@"2"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    self.title=@"We Goal";
    [self LoadingViewElementWeGoal];
    // Do any additional setup after loading the view.
}
-(void)LoadingViewElementWeGoal{
    _Tableview.allowsSelection=NO;
    
    self.AddTaskTF.backgroundColor=[UIColor clearColor];
    self.AddTaskTF.layer.cornerRadius=2;
    self.AddTaskTF.layer.borderWidth=1;
    self.AddTaskTF.layer.borderColor=[UIColor whiteColor].CGColor;
    
    
    self.AddGuideTF.backgroundColor=[UIColor clearColor];
    self.AddGuideTF.layer.cornerRadius=2;
    self.AddGuideTF.layer.borderWidth=1;
    self.AddGuideTF.layer.borderColor=[UIColor whiteColor].CGColor;
    
    
    [self.submitbuttoninstance setExclusiveTouch:YES];
    [self.submitbuttoninstance.layer setMasksToBounds:YES];
    [self.submitbuttoninstance.layer setCornerRadius:5.0f];
    self.submitbuttoninstance.backgroundColor = [UIColor colorWithHex:@"3ac3f9"];
    
    [self.FirendSelectButtonInstance setExclusiveTouch:YES];
    [self.FirendSelectButtonInstance.layer setMasksToBounds:YES];
    [self.FirendSelectButtonInstance.layer setCornerRadius:5.0f];
    self.FirendSelectButtonInstance.layer.borderWidth=1.5f;
    self.FirendSelectButtonInstance.layer.borderColor=[UIColor colorWithHex:@"3ac3f9"].CGColor;
    self.FirendSelectButtonInstance.backgroundColor = [UIColor clearColor];
    
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showSelectedDate)];
    [doneBtn setTintColor:[UIColor colorWithHex:@"2E3992"]];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.StartDateTF setInputAccessoryView:toolBar];
    [self.StartDateTF setInputView:datePicker];
    [self.EndDateTF setInputAccessoryView:toolBar];
    [self.EndDateTF setInputView:datePicker];
    [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    datePicker.backgroundColor=[UIColor colorWithHex:@"2E3992"];
    [_Tableview setNeedsDisplay];
}
#pragma mark  DatePicker Methods
-(void) dateTextField:(id)sender{
    if (TagCount == 3) {
        UIDatePicker *picker = (UIDatePicker*)self.StartDateTF.inputView;
        eventDateStart = picker.date;
        dateString = [self.dateFormat stringFromDate:eventDateStart];
        [picker setMinimumDate:[NSDate date]];
        stratDateString = [self.dateFormat stringFromDate:eventDateStart];
       // self.StartDateTF.text = [NSString stringWithFormat:@"%@",dateString];
        NSString *datestring2=[self.SecDateformat stringFromDate:eventDateStart];
        self.StartDateTF.text=[NSString stringWithFormat:@"%@",datestring2];
    }else if (TagCount == 4){
        UIDatePicker *picker = (UIDatePicker*)self.EndDateTF.inputView;
        [picker setMinimumDate:[self.dateFormat dateFromString:stratDateString]];
        eventDateEnd = picker.date;
       // NSString *dateString12 = [self.dateFormat stringFromDate:eventDateEnd];
        endDateString = [self.dateFormat stringFromDate:eventDateEnd];
        NSString *datestring2=[self.SecDateformat stringFromDate:eventDateEnd];
        self.EndDateTF.text = [NSString stringWithFormat:@"%@",datestring2];
    }
}
- (BOOL)textFieldShouldBeginEditing:(ACFloatingTextField *)textField {
    // Show UIPickerView
    TagCount =textField.tag;
    NSLog(@"Get Tag %li",(long)TagCount);
    return YES;
}
-(void)showSelectedDate{
    [self.StartDateTF resignFirstResponder];
    [self.EndDateTF resignFirstResponder];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, 250, 0);
    self.scroll_view.contentInset = contentInsets;
    self.scroll_view.scrollIndicatorInsets = contentInsets;
}
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scroll_view.contentInset = contentInsets;
    self.scroll_view.scrollIndicatorInsets = contentInsets;
}
#pragma mark  UITextfield Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    TagCount =textField.tag;
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 3) {
        [textField resignFirstResponder];
    }else{
        [self jumpToNextTextField:textField withTag:nextTag];
    }
    return NO;
}
- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [TitleData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    WeGuideLineCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WeGuideLineCellTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
     _Tableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"WeGuideLineCellTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.Delete_Icon.tag=indexPath.row;
    [cell.Delete_Icon addTarget:self action:@selector(DeleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.GuideLineText.text=[TitleData objectAtIndex:indexPath.row];
    return cell;
}
-(void)DeleteButtonClick:(UIButton*)sender{
    [TitleData removeObjectAtIndex:sender.tag];
    [_Tableview reloadData];
}
- (IBAction)AddGuide:(id)sender {
    if (_AddGuideTF.text.length==0) {
        [self validationErrorAlert:@"Please, Fill the details."];
    }else{
        [TitleData addObject:_AddGuideTF.text];
        self.AddGuideTF.text=@"";
        [_Tableview reloadData];
    }
}
#pragma mark  Alert Prompt Method
-(void)validationErrorAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                    message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    int duration = 1; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
#pragma mark  CandidateRole Delegate
-(void)sendCandidateRolesToAddCandidate:(NSMutableArray *)selectedRoles{
    NSLog(@"Selected Candidate Role ID's %@",selectedRoles);
    selectedCandidateRolesArray=selectedRoles;
    NSString *Count = [NSString stringWithFormat:@"%lu People Selected",(unsigned long)[selectedCandidateRolesArray count]];
    [_FirendSelectButtonInstance setTitle:Count forState:UIControlStateNormal];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"candRoleSegueFromAddBooking"]){
        WeGoalPeopleViewController * candRoleVC = [segue destinationViewController];
        candRoleVC.delegate=self;
        candRoleVC.selectedArray=selectedCandidateRolesArray;
    }
}
- (IBAction)Submit:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        NSComparisonResult result;
        result = [eventDateStart compare:eventDateEnd];
        if(([self.AddTaskTF.text isEqualToString:@""]) && ([self.AddGuideTF.text isEqualToString:@""]) && ([self.StartDateTF.text isEqualToString:@""]) && ([self.EndDateTF.text isEqualToString:@""])){
            [self validationErrorAlert:@"Please, Fill all the details."];
        }
        else if ([self.AddTaskTF.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Add Your Task."];
        }else if ([self.AddGuideTF.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Add Your Guide Line."];
        }else if ([self.StartDateTF.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Select Start Date"];
        }else if ([self.EndDateTF.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Select End Date"];
        }else if (result == NSOrderedDescending){
            [self validationErrorAlert:@"Please, Select Valide End Date"];
        }else if([selectedCandidateRolesArray count]==0){
            [self validationErrorAlert:@"Please, Select Invite User"];
        }else{
            NSString *InviteUserId = [selectedCandidateRolesArray componentsJoinedByString:@","];
            NSLog(@"Select User Id %@",InviteUserId);
            NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            NSLog(@"Store User Id %@",UserId);
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:TitleData options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"FriendList%@", jsonString);
            NSString *FinalString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSLog(@"Final String %@",FinalString);
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSDictionary *dictParam = @{@"userId":UserId,@"Task":self.AddTaskTF.text,@"GuideLine":self.AddGuideTF.text,@"StartDate":stratDateString,@"EndDate":endDateString,@"inviteUserId":InviteUserId};
            NSString *loginURL=[NSString stringWithFormat:@"%@addWeGoal",KBaseUrl];
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"response:%@",response);
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                        });
                        NSString *Goalie = [response valueForKey:@"goalie"];
                        NSString *SubTitle = [NSString stringWithFormat:@"%@ Goalies needed to complete your Goal.",Goalie];
                        self.alertImage = [UIImage imageNamed:@"GoliesIcon"];
                        [self showAlertInView:self withTitle:self.AddTaskTF.text withSubtitle:SubTitle withCustomImage:self.alertImage withDoneButtonTitle:@"Done" andButtons:nil];
                    });
                }else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"User name or password is incorrect" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
                else if (response==NULL) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    NSLog(@"response is null");
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (void) showAlertInView:(UIViewController *)view withTitle:(NSString *)title withSubtitle:(NSString *)subTitle withCustomImage:(UIImage *)image withDoneButtonTitle:(NSString *)done andButtons:(NSArray *)buttons {
    FCAlertView *alert = [[FCAlertView alloc] init];
    self.alertTitle = title;
    alert.subTitleColor = [self checkFlatColors:subtitleColor];
    alert.blurBackground = 1;
    alert.bounceAnimations = 1;
    alert.darkTheme = YES;
    alert.fullCircleCustomImage = YES;
    alert.delegate = self;
    //[alert makeAlertTypeSuccess];
    alert.avoidCustomImageTint = 1;
    alert.subtitleFont = [UIFont fontWithName:@"Avenir" size:17.0];
    self.arrayOfButtonTitles = @[];
    [alert showAlertInView:view withTitle:title withSubtitle:subTitle withCustomImage:image withDoneButtonTitle:done andButtons:buttons];
}
- (UIColor *) checkFlatColors:(NSString *)selectedColor {
    FCAlertView *alert = [[FCAlertView alloc] init];
    color = alert.flatTurquoise;
    return color;
}
-(void)FCAlertDoneButtonClicked:(FCAlertView *)alertView{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
