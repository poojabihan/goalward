//
//  WeGoalCreateViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import <FCAlertView.h>
@interface WeGoalCreateViewController : UIViewController


@property (strong, nonatomic) IBOutlet ACFloatingTextField *AddTaskTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *AddGuideTF;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll_view;

@property (strong, nonatomic) IBOutlet UIButton *FirendSelectButtonInstance;
@property (strong, nonatomic) IBOutlet UITableView *Tableview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableviewlayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *datelayout;
@property (strong, nonatomic) IBOutlet UIView *ScrollContainView;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *StartDateTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *EndDateTF;
@property (strong, nonatomic) IBOutlet UIButton *submitbuttoninstance;

- (IBAction)AddGuide:(id)sender;

- (IBAction)Submit:(id)sender;
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView;
@end
