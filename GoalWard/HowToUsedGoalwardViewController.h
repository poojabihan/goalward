//
//  HowToUsedGoalwardViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 10/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowToUsedGoalwardViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (strong, nonatomic) IBOutlet UITableView *tableview;



@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward1;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward2;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward3;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward4;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward5;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward6;
@property (strong, nonatomic) IBOutlet UILabel *WelComeGoalward7;
@property (strong, nonatomic) IBOutlet UILabel *title1;
@property (strong, nonatomic) IBOutlet UILabel *title2;
@property (strong, nonatomic) IBOutlet UILabel *title3;
@property (strong, nonatomic) IBOutlet UILabel *title4;
@property (strong, nonatomic) IBOutlet UILabel *title5;
@property (strong, nonatomic) IBOutlet UILabel *title6;
@property (strong, nonatomic) IBOutlet UILabel *title7;


@end
