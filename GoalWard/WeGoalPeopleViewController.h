//
//  WeGoalPeopleViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol sendCandidateRoleDataProtocol <NSObject>

-(void)sendCandidateRolesToAddCandidate:(NSMutableArray *)selectedRoles;

@end
@interface WeGoalPeopleViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)Done_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *search_Bar;

@property(nonatomic,assign)id delegate;
@property (nonatomic,strong)NSString*updateCheck;
@property NSMutableArray * candidateRoleNamesArray;
@property NSMutableArray * selectedArray;

@end
