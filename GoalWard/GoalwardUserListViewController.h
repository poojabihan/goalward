//
//  GoalwardUserListViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 07/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalwardUserListViewController : UIViewController


@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;



@end
