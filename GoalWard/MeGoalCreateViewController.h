//
//  MeGoalCreateViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import <FCAlertView.h>
@interface MeGoalCreateViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableviewlayout;
@property (strong, nonatomic) IBOutlet UIView *ScrollContinView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *datelayout;
- (IBAction)AddGuidLineButtonAction:(id)sender;
@property NSString *EditMeTaskCheck;
@property NSArray *EditDateArray;
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView;
@end
