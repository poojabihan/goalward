//
//  GoalwardUserListViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 07/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "GoalwardUserListViewController.h"
#import "GoalwardUserListTableViewCell.h"
#import "Webservice.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"
@interface GoalwardUserListViewController ()
{
    MBProgressHUD *hud;NSMutableArray *ListData;NSArray *searchResults;
    BOOL searchEnabled;NSDictionary *dictParam;NSString *UserID;
    MBProgressHUD *RequestHud;
}
@end
@implementation GoalwardUserListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.SearchBar setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    [self.SearchBar setBarTintColor:[UIColor colorWithHex:@"0b122f"]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    _TableView.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    self.TableView.tableFooterView = [UIView new];

    [self servicesLoadingUserList];
}
-(void)servicesLoadingUserList{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":UserId};
        NSLog(@"Dic %@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@allUsers",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            
            if (([[response objectForKey:@"status"]boolValue] == 1)){
                ListData = [response valueForKey:@"data"];
                NSLog(@"%@",ListData);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[_TableView reloadData];
                    [self.TableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [hud hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    self.TableView.hidden=YES;
                }else{
                    self.TableView.hidden=NO;
                }
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        return [searchResults count];
    }else{
        return [ListData count];
    }
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    GoalwardUserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoalwardUserListTableViewCell"];
    cell.ProfilePhoto.layer.cornerRadius=cell.ProfilePhoto.frame.size.width/2;
    cell.ProfilePhoto.layer.masksToBounds = YES;
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.AddFriendsBtn.layer.cornerRadius=5;
    cell.BAckView.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    [cell.BAckView.layer setCornerRadius:5.0f];
    [cell.BAckView.layer setBorderColor:[UIColor clearColor].CGColor];
    [cell.BAckView.layer setBorderWidth:0.2f];
    [cell.BAckView.layer setShadowColor:[UIColor clearColor].CGColor];
    [cell.BAckView.layer setShadowOpacity:5.0];
    [cell.BAckView.layer setShadowRadius:5.0];
    [cell.BAckView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"GoalwardUserListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.accessoryView = activityIndicator;
    }
    
     if (searchEnabled) {
         cell.UserName.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"name"]objectAtIndex:indexPath.row]];
         
         NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
         
         if ([SocialCheck isEqualToString:@"Y"]) {
             
             [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
             
         }else{
             
             [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[searchResults valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
         }
         cell.AddFriendsBtn.tag = indexPath.row;
         [cell.AddFriendsBtn addTarget:self action:@selector(AddFriendsBtnClick:) forControlEvents:UIControlEventTouchUpInside];
         NSString *Status = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"status"]objectAtIndex:indexPath.row]];
         if ([Status isEqualToString:@"pending"]) {
             [ cell.AddFriendsBtn setTitle:@"Pending" forState:UIControlStateNormal];
         }else{
             [cell.AddFriendsBtn setTitle:@"Add Friend" forState:UIControlStateNormal];
         }
     }else{
         cell.UserName.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"name"]objectAtIndex:indexPath.row]];
         
         NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
         
         if ([SocialCheck isEqualToString:@"Y"]) {
             
             [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
             
         }else{
             
             [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
         }
         cell.AddFriendsBtn.tag = indexPath.row;
         [cell.AddFriendsBtn addTarget:self action:@selector(AddFriendsBtnClick:) forControlEvents:UIControlEventTouchUpInside];
         NSString *Status = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"status"]objectAtIndex:indexPath.row]];
         if ([Status isEqualToString:@"pending"]) {
             [ cell.AddFriendsBtn setTitle:@"Pending" forState:UIControlStateNormal];
         }else{
             [cell.AddFriendsBtn setTitle:@"Add Friend" forState:UIControlStateNormal];
         }
     }
    return cell;
}
-(void)AddFriendsBtnClick:(UIButton*)sender
{
    if (searchEnabled){
        UserID = [[searchResults valueForKey:@"userId"] objectAtIndex:sender.tag];
        NSLog(@"Search Request Send Id %@",UserID);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addfriendwebservicescall];
        });
    }else{
        UserID = [[ListData valueForKey:@"userId"] objectAtIndex:sender.tag];
        NSLog(@"Without Search Request Send Id %@",UserID);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addfriendwebservicescall];
        });
    }
}
-(void)addfriendwebservicescall{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        RequestHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        RequestHud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        RequestHud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        RequestHud.contentColor = [UIColor whiteColor];
        RequestHud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":LoginUserId,@"friendId":UserID};
        NSLog(@"%@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@addFriend",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
        NSLog(@"response:%@",response);
        if (([[response objectForKey:@"status"]boolValue] == 1)){
            [self servicesLoadingUserList];
            [RequestHud hideAnimated:YES];
        }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [RequestHud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
        }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [RequestHud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [RequestHud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    //1. Define the initial state (Before the animation)
//    cell.transform = CGAffineTransformMakeTranslation(0.f, 100);
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//    //2. Define the final state (After the animation) and commit the animation
//    [UIView beginAnimations:@"rotation" context:NULL];
//    [UIView setAnimationDuration:0.5];
//    cell.transform = CGAffineTransformMakeTranslation(0.f, 0);
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
//}
- (void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    if (airlineName == nil) {
        // If empty the search results are the same as the original data
        searchResults = [ListData mutableCopy];
    } else {
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }
        else if ([scope isEqualToString:@"1"]){
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }
    }
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.TableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
        
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length==0) {
        [searchBar resignFirstResponder];
    }else{
        [searchBar resignFirstResponder];
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.TableView reloadData];
    
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}
@end
