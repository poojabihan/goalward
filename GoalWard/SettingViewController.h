//
//  SettingViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface SettingViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *ProfileBackView;
@property (strong, nonatomic) IBOutlet UIImageView *ProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *UserName;
@property (strong, nonatomic) IBOutlet UIView *FirstView;
@property (strong, nonatomic) IBOutlet UIView *SecondView;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *FirstBigView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SecondBigView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *Scrollview;

- (IBAction)UpdateProfile:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *ProfileUpdateButton;

@property (strong, nonatomic) IBOutlet UITableView *tablrview;



@end
