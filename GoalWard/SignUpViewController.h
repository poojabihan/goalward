//
//  SignUpViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface SignUpViewController : UIViewController


@property (strong, nonatomic) IBOutlet ACFloatingTextField *UserNameTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *EmailTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *ContactTF;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ContactLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *CountryLayout;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *PassWordTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *ConformPasswordTF;
@property (strong, nonatomic) IBOutlet UIButton *CountryButtionInstance;

@property (strong, nonatomic) IBOutlet UIButton *SignUpButtonInstance;

- (IBAction)Submit_button_Click:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@end
