//
//  CommentHistoryViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface CommentHistoryViewController : UIViewController


@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property NSString *EventID;
@property NSMutableArray *allUsersArr;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtComment;
@property BOOL isFromNotifications;
-(void)refreshView:(NSString *)eventID;



@end
