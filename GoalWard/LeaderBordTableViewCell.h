//
//  LeaderBordTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderBordTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UIImageView *Profile_Image;

@property (strong, nonatomic) IBOutlet UIView *BackgroundView;

@property (strong, nonatomic) IBOutlet UILabel *UserName;

@property (strong, nonatomic) IBOutlet UILabel *RatingPercentage;

@property (strong, nonatomic) IBOutlet UILabel *RatingStar;

@end
