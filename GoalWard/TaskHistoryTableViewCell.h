//
//  TaskHistoryTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 24/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskHistoryTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *GuidlineLabel;

@property (strong, nonatomic) IBOutlet UIImageView *ChekImage;


@property (strong, nonatomic) IBOutlet UILabel *EarnGloiest;


@end
