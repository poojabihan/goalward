//
//  ProfileUpdateViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 31/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "ProfileUpdateViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import <AFHTTPSessionManager.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"
#import "NotificationTableViewCell.h"
#import "MyGoalRewardViewController.h"
#import "CommentHistoryViewController.h"
#import "CommentViewController.h"
#import "AllUserAndPendingRequestViewController.h"

@interface ProfileUpdateViewController ()
{
    MBProgressHUD *hud;NSMutableArray *ListData;UIRefreshControl *refreshController;NSString *UserId;
}
@end

@implementation ProfileUpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableview.tableFooterView = [UIView new];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.tableview setEstimatedRowHeight:90];
  
//    UIBarButtonItem *DeleteButton = [[UIBarButtonItem alloc]
//                                     initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
//                                     target:self
//                                     action:@selector(searchItem:)];
//
//    self.navigationItem.rightBarButtonItem = DeleteButton;

//    nc.navigationController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(Add:)];

    
    ListData = [[NSMutableArray alloc] init];
    
    _tableview.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    
    // _tableview.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor=[UIColor whiteColor];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    
    //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing Data...."];
    
    [_tableview addSubview:refreshController];
    
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self NotificationListWebservices];
    });
   
}

-(void)searchItem:(UIBarButtonItem*)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
         [self deleteServices];
    });
}
-(void)deleteServices{
     NSLog(@"Delete Success");
}

#pragma mark - Handle Refresh Method
-(void)handleRefresh : (id)sender{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self NotificationListWebservices];
    });
    
}
-(void)NotificationListWebservices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        
        UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSDictionary *dictParam = @{@"userId":UserId};
        NSString * loginURL=[NSString stringWithFormat:@"%@getNotification",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                ListData = [response valueForKey:@"result"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableview reloadData];
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES];
                });
                NSLog(@"Response Success");
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ListData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationTableViewCell"];
    cell.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.UserProfile.layer.cornerRadius = cell.UserProfile.frame.size.width/2;
    cell.UserProfile.layer.masksToBounds = YES;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.NotificationText.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"notification"]objectAtIndex:indexPath.row]];
    
    cell.DateAndTime.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"created_at"]objectAtIndex:indexPath.row]];
    
    NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
    
    if ([SocialCheck isEqualToString:@"Y"]) {
        
        [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }else{
        
        [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"TaskIncomplete"]) {
        
        MyGoalRewardViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
        controler.Indexarray=[ListData objectAtIndex:indexPath.row];
        controler.TaskID = [[ListData valueForKey:@"event_id"] objectAtIndex:indexPath.row];
        controler.isEditEndDate = YES;
        [self.navigationController pushViewController:controler animated:YES];
    }
    if ([[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"ShowComment"]) {
        
        CommentHistoryViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentHistoryViewController"];
        controler.EventID = [[ListData valueForKey:@"event_id"] objectAtIndex:indexPath.row];
        controler.isFromNotifications = YES;
        [self.navigationController pushViewController:controler animated:YES];
    }
    if ([[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"seeprogress"]) {
        
        CommentViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
        controler.EventID = [[ListData valueForKey:@"event_id"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controler animated:YES];
    }
    if ([[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"TaskStart"]) {
        
        MyGoalRewardViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
        controler.TaskID = [[ListData valueForKey:@"event_id"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controler animated:YES];
    }
    if ([[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"friendlist"] || [[[ListData valueForKey:@"notification_view"]objectAtIndex:indexPath.row] isEqualToString:@"PendingViewList"]) {
        
        AllUserAndPendingRequestViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"AllUserAndPendingRequestViewController"];
        [self.navigationController pushViewController:controler animated:YES];
    }
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
@end
