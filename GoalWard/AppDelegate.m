//
//  AppDelegate.m
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ViewController.h"
#import "TabBarViewController.h"
@import Firebase;
@import FirebaseMessaging;
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterSmokeStyle.h"
#import "Reachability.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AddGoalViewController.h"


#import "AllUserAndPendingRequestViewController.h"
#import "CommentViewController.h"
#import "CommentHistoryViewController.h"
#import "ComplitedTaskViewController.h"
#import "MyGoalRewardViewController.h"
@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
    NSString * refreshedToken;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSUserDefaults *store=[NSUserDefaults standardUserDefaults];
    
    if (![store valueForKey:@"Login"])
    {
        [store setObject:@"0" forKey:@"Login"];
    }
    
    NSLog(@"Store Value Check %@",[store valueForKey:@"Login"]);
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([[store valueForKey:@"Login"]isEqualToString:@"0"])
    {
        //login
        ViewController *login = (ViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
        self.window.rootViewController=nav;
    }
    else
    {
        //home
        TabBarViewController *vc=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        self.window.rootViewController=vc;
    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
#pragma mark:Notification Delegate
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tokenRefreshCallBack:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        // [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
        
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * error)
         {
             if (granted==NO) {
                 NSLog(@"Didnt allowed");
                 UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Notification Services Disabled!"  message:@"Please open this app's settings enable Notifications to get important Updates!"  preferredStyle:UIAlertControllerStyleAlert];
                 [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                             {
                                                 @try
                                                 {
                                                     NSLog(@"tapped Settings");
                                                     BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                                     if (canOpenSettings)
                                                     {
                                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                         
                                                         
                                                         [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                                                     }
                                                 }
                                                 @catch (NSException *exception)
                                                 {
                                                     
                                                 }
                                             }]];
                 [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
                 UINavigationController *nvc = (UINavigationController *)[[application windows] objectAtIndex:0].rootViewController;
                 UIViewController *vc = nvc.visibleViewController;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [vc presentViewController:alertController animated:YES completion:nil];
                 });
                 
             }
             if(error)
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     [[UIApplication sharedApplication] registerForRemoteNotifications];
                 });
                 // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
         }];
 
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    return YES;
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:app
                                                                  openURL:url
                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                    annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber=0)];
//    UILocalNotification *notification=[[UILocalNotification alloc]init];
//    notification.applicationIconBadgeNumber=-1;
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [[FIRMessaging messaging]setShouldEstablishDirectChannel:NO];
    NSLog(@"Disconnected From FCM");
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber=0)];
    //UILocalNotification *notification=[[UILocalNotification alloc]init];
    //notification.applicationIconBadgeNumber=-1;
    //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [self connectToFirebase];

}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    
    NSLog(@"Failed to get token, error: %@", error.localizedDescription);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {
    NSLog(@"Message Id: %@",userInfo[@"gcm.message_id"]);
    NSLog(@"Message %@",userInfo);
    NSLog(@"Notification Delivered");
    
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    /*
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        if ([userInfo objectForKey:@"changeit_id"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
                //  UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                AddGoalViewController * chatVC=(AddGoalViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"AddGoalViewController"];
                [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
                handler( UIBackgroundFetchResultNewData );
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
               UITabBarController *tab2 = (UITabBarController *)self.window.rootViewController;
                UIStoryboard *storyBoard2 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                AddGoalViewController * chatVC2=(AddGoalViewController *)[storyBoard2 instantiateViewControllerWithIdentifier:@"AddGoalViewController"];
                [(UINavigationController *)tab2.selectedViewController pushViewController:chatVC2 animated:YES];
                handler( UIBackgroundFetchResultNewData );
             
            });
            
        }
        
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        handler( UIBackgroundFetchResultNewData );
    }
    else
    {
        NSLog( @"FOREGROUND" );
        if ([userInfo objectForKey:@"changeit_id"]) {
            [self playNotificationSound];
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterSmokeStyle new];
            [JCNotificationCenter
             enqueueNotificationWithTitle:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"title"]
             message:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"body"]
             tapHandler:^{
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                   
                     UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
                     //  UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                     UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     AddGoalViewController * chatVC=(AddGoalViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"AddGoalViewController"];
                     [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
                     handler( UIBackgroundFetchResultNewData );
                 });
             }];
        }
        else{
     
            dispatch_async(dispatch_get_main_queue(), ^{
                UITabBarController *tab2 = (UITabBarController *)self.window.rootViewController;
                //  UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                AddGoalViewController * chatVC=(AddGoalViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"AddGoalViewController"];
                [(UINavigationController *)tab2.selectedViewController pushViewController:chatVC animated:YES];
                handler( UIBackgroundFetchResultNewData );
                
                 [self playNotificationSound];
                
            });
            
        }
        
    }*/
}
-(void)playNotificationSound{
    //play sound
    SystemSoundID    pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms_alert_aurora" ofType:@"caf"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    // custom code to handle push while app is in the foreground
    
    NSLog(@"%@", notification.request.content.userInfo);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^_Nonnull __strong)(void))completionHandler
{
    
    NSString *OpenViewWithCondition = [response.notification.request.content.userInfo objectForKey:@"NotificationView"];
    
    NSLog(@"open view value - %@   - - - %@",OpenViewWithCondition,response);
    
    
    if ([OpenViewWithCondition isEqualToString:@"PendingViewList"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
          
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            AllUserAndPendingRequestViewController * chatVC=(AllUserAndPendingRequestViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"AllUserAndPendingRequestViewController"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
        });
        
    }else if ([OpenViewWithCondition isEqualToString:@"friendlist"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            AllUserAndPendingRequestViewController * chatVC=(AllUserAndPendingRequestViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"AllUserAndPendingRequestViewController"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
        });
        
    }else if ([OpenViewWithCondition isEqualToString:@"seeprogress"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            CommentViewController * chatVC=(CommentViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"CommentViewController"];
            chatVC.EventID=[response.notification.request.content.userInfo valueForKey:@"event_id"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
            
        });

//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.EventIDCheckAppDelegate=YES;
//            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
//            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//            CommentViewController * chatVC=(CommentViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"CommentViewController"];
//            chatVC.EventID=[response.notification.request.content.userInfo valueForKey:@"event_id"];
//            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
//            completionHandler();
//            
//        });

        
    }else if ([OpenViewWithCondition isEqualToString:@"ShowComment"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;

            if ([[(UINavigationController *)tab.selectedViewController topViewController] isKindOfClass:[CommentHistoryViewController class]]) {
                
                CommentHistoryViewController * chatVC = [(UINavigationController *)tab.selectedViewController topViewController];
                [chatVC refreshView:[response.notification.request.content.userInfo valueForKey:@"event_id"]];
            }
            else
            {
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            CommentHistoryViewController * chatVC=(CommentHistoryViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"CommentHistoryViewController"];
            chatVC.isFromNotifications = YES;
            chatVC.EventID=[response.notification.request.content.userInfo valueForKey:@"event_id"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
        }
        });
        
    }else if ([OpenViewWithCondition isEqualToString:@"TaskStart"]){
        //MyGoalRewardViewController
        dispatch_async(dispatch_get_main_queue(), ^{
            self.EventIDCheckAppDelegate=YES;
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            MyGoalRewardViewController * chatVC=(MyGoalRewardViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
            chatVC.TaskID=[response.notification.request.content.userInfo valueForKey:@"event_id"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
            
        });
    }else if ([OpenViewWithCondition isEqualToString:@"TaskIncomplete"]){
        //MyGoalRewardViewController
        dispatch_async(dispatch_get_main_queue(), ^{
            self.EventIDCheckAppDelegate=YES;
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            MyGoalRewardViewController * chatVC=(MyGoalRewardViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
            chatVC.isEditEndDate = YES;
            chatVC.TaskID=[response.notification.request.content.userInfo valueForKey:@"event_id"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
            
        });
        
    }
    else if ([OpenViewWithCondition isEqualToString:@"TaskComplite"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.EventIDCheckAppDelegate=YES;
            UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            ComplitedTaskViewController * chatVC=(ComplitedTaskViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"ComplitedTaskViewController"];
            [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            completionHandler();
            
        });
        
    }
    
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation{
    
    return YES;
    
}

#pragma mark : Firebase Delegate

-(void)tokenRefreshCallBack:(NSNotification *)notification{
    refreshedToken=[[FIRInstanceID instanceID]token];
    NSLog(@"InstanceId Token %@",refreshedToken);
    //Connect to FCM
    [self connectToFirebase];
    
}

-(void)connectToFirebase{
    
    NSError *error;
    
    [[FIRMessaging messaging]setShouldEstablishDirectChannel:YES];

        if (error!=nil) {
            NSLog(@"Unable to Connect to FCM %@",error.localizedDescription);
        }else{
            NSLog(@"Connected to FCM");

        };
}
- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"instanceId_notification=>%@",[notification object]);
    NSString * instanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSLog(@"String %@",instanceID);
    [self connectToFirebase];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"GoalWard"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
