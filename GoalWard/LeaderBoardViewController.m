//
//  LeaderBoardViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "LeaderBoardViewController.h"
#import "LeaderBordTableViewCell.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"
@interface LeaderBoardViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    MBProgressHUD *hud;NSMutableArray *ListData;UIRefreshControl *refreshController;NSString *UserId;
}
@end
@implementation LeaderBoardViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    ListData = [[NSMutableArray alloc] init];
    
    _tableview.backgroundColor=[UIColor colorWithHex:@"0b122f"];
   // _tableview.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor=[UIColor whiteColor];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing Data...."];
    [_tableview addSubview:refreshController];
    self.tableview.tableFooterView = [UIView new];
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
     [self loadingElement123];
    // Do any additional setup after loading the view.
}
#pragma mark - Handle Refresh Method
-(void)handleRefresh : (id)sender{
    [self loadingElement123];
}
-(void)loadingElement123{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSDictionary *dictParam = @{@"userId":UserId};
        NSString * loginURL=[NSString stringWithFormat:@"%@getMyEventLeaderboard",KBaseUrl];

        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                ListData = [response valueForKey:@"Data"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[_tableview reloadData];
                    [_tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES];
                });
                NSLog(@"Response Success");
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [refreshController endRefreshing];
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ListData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    LeaderBordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeaderBordTableViewCell"];
    cell.Profile_Image.layer.cornerRadius = cell.Profile_Image.frame.size.width/2;
    cell.Profile_Image.layer.masksToBounds = YES;
    cell.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"LeaderBordTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
//================ User Index HightLight ====================
//    NSString *localUser = [NSString stringWithFormat:@"%@",UserId];
//    NSString *UserCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"userId"]objectAtIndex:indexPath.row]];
//    if ([localUser isEqualToString:UserCheck]) {
//        cell.backgroundColor=[UIColor blackColor];
//        cell.BackgroundView.backgroundColor=[UIColor blackColor];
//    }else{
//         cell.backgroundColor=[UIColor whiteColor];
//    }
    
    cell.UserName.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"name"]objectAtIndex:indexPath.row]];
    cell.RatingPercentage.text=[NSString stringWithFormat:@"%@ %%",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
    cell.RatingStar.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"reward"]objectAtIndex:indexPath.row]];
    
    
    
    NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
    
    if ([SocialCheck isEqualToString:@"Y"]) {
        
        [cell.Profile_Image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
        
    }else{
        
        [cell.Profile_Image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }
    
   // [cell.Profile_Image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[ListData valueForKey:@"image"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //1. Define the initial state (Before the animation)
    cell.transform = CGAffineTransformMakeTranslation(0.f, 100);
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    //2. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.5];
    cell.transform = CGAffineTransformMakeTranslation(0.f, 0);
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
@end
