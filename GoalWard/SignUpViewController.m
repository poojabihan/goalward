//
//  SignUpViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "SignUpViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import <AFHTTPSessionManager.h>
@import FirebaseInstanceID;
@import Firebase;
@interface SignUpViewController ()<UITextFieldDelegate,UIScrollViewDelegate>
{
    MBProgressHUD *HUD;
    NSString *countryCode12;  NSUserDefaults *Store;
}
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.ContactLayout.constant=0;
    self.CountryLayout.constant=0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [self LoadingViewElement];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)LoadingViewElement{

//    self.UserNameTF.backgroundColor=[UIColor whiteColor];
//    self.EmailTF.backgroundColor=[UIColor whiteColor];
//    self.PassWordTF.backgroundColor=[UIColor whiteColor];
//    self.ConformPasswordTF.backgroundColor=[UIColor whiteColor];
    
    [self.SignUpButtonInstance setExclusiveTouch:YES];
    [self.SignUpButtonInstance.layer setMasksToBounds:YES];
    [self.SignUpButtonInstance.layer setCornerRadius:5.0f];
    //self.SignUpButtonInstance.backgroundColor = [UIColor whiteColor];
    
    [self.CountryButtionInstance setExclusiveTouch:YES];
    [self.CountryButtionInstance.layer setMasksToBounds:YES];
    [self.CountryButtionInstance.layer setCornerRadius:20.0f];
//    self.CountryButtionInstance.backgroundColor = [UIColor whiteColor];
    
}

#pragma mark  Keyboard Delegates

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
//UIKeyboardFrameBeginUserInfoKey

- (void)keyboardWasShown:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, 320, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
#pragma mark  UITextfield Delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
       
        [nextResponder becomeFirstResponder];
    }
    else {

        [textField resignFirstResponder];
    }
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField == self.nameTF||textField==self.surnameTF||textField==self.postCodeTF||self.telephoneTF||self.secondaryNumberTF||self.emailTF||self.TeacherReferenceNumberTF) {
//        
//        // do not allow the first character to be space | do not allow more than one space
//        if ([string isEqualToString:@" "]) {
//            if (!textField.text.length)
//                return NO;
//            
//        }
//        
//        
//    }
//    return YES;
//    
//}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


- (IBAction)Submit_button_Click:(id)sender {
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        
        if((([self.UserNameTF.text isEqualToString:@""])) && ([self.EmailTF.text isEqualToString:@""]) && ([self.ContactTF.text isEqualToString:@""]) && ([self.PassWordTF.text isEqualToString:@""]) && ([self.ConformPasswordTF.text isEqualToString:@""]))
        {
            
            [self validationErrorAlert:@"Please, Fill all the details."];
            
        }
        
        else if ([self.UserNameTF.text isEqualToString:@""])
        {
            
            [self validationErrorAlert:@"Please, Fill User Name"];
            
        }
        else if ([self.EmailTF.text isEqualToString:@""])
        {
            
            [self validationErrorAlert:@"Please, Fill Email Address"];
            
        }
        else if(![self NSStringIsValidEmail:self.EmailTF.text])
        {
            
            [self validationErrorAlert:@"Invalid e-mail address!"];
            
        }
        //---------country Button-------
        
//        else if ([self.CountryButtionInstance.titleLabel.text isEqualToString:@"Select Country"])
//        {
//            [self validationErrorAlert:@"Please Select Country!"];
//        }
        //-----------------------------
        
        else if ([self.PassWordTF.text isEqualToString:@""])
        {
            
            [self validationErrorAlert:@"Please, Fill Password"];
            
        }
        
        else if ([self.ConformPasswordTF.text isEqualToString:@""])
        {
            
            [self validationErrorAlert:@"Please, Fill Confirmed Password"];
            
        }
        
        else if (![self.PassWordTF.text isEqualToString:self.ConformPasswordTF.text])
        {
            
            [self validationErrorAlert:@"Passwords do not match"];
            
        }
        
        else
        {
            
            HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            HUD.contentColor = khudColour;
            
            // Set the label text.
            HUD.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            
            NSString *FCMToken = [[FIRInstanceID instanceID]token];
            
            NSDictionary *dictParam = @{@"name":self.UserNameTF.text,@"email":self.EmailTF.text,@"password":self.PassWordTF.text,@"device_token":FCMToken};
            
            NSString *loginURL=[NSString stringWithFormat:@"%@userRegister",KBaseUrl];
            
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                
                NSLog(@"response:%@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        NSLog(@"Response Success");
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"email"] forKey:@"email"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"userId"] forKey:@"User_Id"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"name"] forKey:@"UserName"];
                        
                        [Store setObject:@"1" forKey:@"Login"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                        self.navigationController.navigationBarHidden=YES;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUD hideAnimated:YES];
                            [self.navigationController pushViewController:obj animated:YES];
                        });
                        
                    });
                    
                }
                else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideAnimated:YES afterDelay:1];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideAnimated:YES afterDelay:1];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideAnimated:YES afterDelay:1];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                
            } failure:^(NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];

        }
    }
    else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    
}

#pragma mark  CountryPicker Delegate

//- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
//{
//    //self.countryLabel.text = chosenCity.countryName;
//    //self.selectCountryButtonInstance.titleLabel.text=chosenCity.countryName;
//    [self.CountryButtionInstance setTitle:chosenCity.countryName forState: UIControlStateNormal];
//    countryCode12=chosenCity.countryCode;
//
//    NSLog(@"Country Name  %@ Country Code %@",chosenCity.countryName,chosenCity.countryCode);
//    //self.countryPostCodeLabel.text=chosenCity.countryCode;
//    //[self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
//
//}

#pragma mark  PrepareForSegue

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"openCountryPicker"])
//    {
//        EMCCountryPickerController *countryPicker = segue.destinationViewController;
//
//        // default values
//        countryPicker.showFlags = true;
//        countryPicker.countryDelegate = self;
//        countryPicker.drawFlagBorder = true;
//        countryPicker.flagBorderColor = [UIColor grayColor];
//        countryPicker.flagBorderWidth = 0.5f;
//
//    }
//}

@end
