//
//  TaskListMeGoalViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 27/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "TaskListMeGoalViewController.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "TaslListMeGoalTableViewCell.h"
#import "MeGoalCreateViewController.h"
#import "MyGoalRewardViewController.h"
#import "CommentViewController.h"
#import "UIColor+Hexadecimal.h"
@interface TaskListMeGoalViewController (){
    MBProgressHUD *hud;NSMutableArray *ListData;NSArray *searchResults;
    BOOL searchEnabled;NSDictionary *dictParam;
}
@property NSDateFormatter *dateFormat;
@end
@implementation TaskListMeGoalViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
-(void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //_TableView.backgroundColor= [UIColor colorWithHex:@"2E73C0"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.SearchBar setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    [self.SearchBar setBarTintColor:[UIColor colorWithHex:@"0b122f"]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
     _TableView.backgroundColor = [UIColor colorWithHex:@"0b122f"];
    self.view.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    self.TableView.tableFooterView = [UIView new];

//     _TableView.estimatedRowHeight=_TableView.rowHeight;
//     _TableView.rowHeight=UITableViewAutomaticDimension;
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
}
-(void)viewWillAppear:(BOOL)animated{
    
    if (_isPendingTask){
        [self loadPendingTasks];
        self.title = @"Incomplete Task";
    }
    else {
        [self loadingElement];
        self.title = @"Current Task";
    }
}
-(void)loadingElement{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        NSString  *CurrentDate =  [self.dateFormat stringFromDate:[NSDate date]];
        NSDictionary *dictParam1 = @{@"userId":UserId,@"currentDateTime":CurrentDate};
        NSLog(@"Dic %@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@getCurrentEvents",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                ListData = [response valueForKey:@"Data"];
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [_TableView reloadData];
                    [_TableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [hud hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    _TableView.hidden=YES;
                }else{
                    _TableView.hidden=NO;
                }
                NSLog(@"Response Success");
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)loadPendingTasks{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        NSString  *CurrentDate =  [self.dateFormat stringFromDate:[NSDate date]];
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
//        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
//        hud.contentColor = [UIColor whiteColor];
//        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":UserId,@"currentDateTime":CurrentDate};
        NSLog(@"Dic %@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@getIncompleteEvents",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            [hud hideAnimated:YES ];

            NSLog(@"response:%@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1))
            {
                
                ListData = [response valueForKey:@"Data"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[_TableView reloadData];
                    [_TableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [hud hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    _TableView.hidden=YES;
                }else{
                    _TableView.hidden=NO;
                    
                }
                
            }
            else if (response==NULL) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
            
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        return [searchResults count];
    }else{
        return [ListData count];
    }
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    TaslListMeGoalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaslListMeGoalTableViewCell"];
    //cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backview.backgroundColor=[UIColor clearColor];
    [cell.backview.layer setCornerRadius:5.0f];
    [cell.backview.layer setBorderColor:[UIColor clearColor].CGColor];
    [cell.backview.layer setBorderWidth:0.2f];
    [cell.backview.layer setShadowColor:[UIColor clearColor].CGColor];
    [cell.backview.layer setShadowOpacity:5.0];
    [cell.backview.layer setShadowRadius:5.0];
    [cell.backview.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"TaslListMeGoalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (searchEnabled) {
        cell.Task_Start_Date.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
        cell.Task_End_Date.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"endDate"] objectAtIndex:indexPath.row]];
        cell.User_Task.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"event"] objectAtIndex:indexPath.row]];
        cell.TotalGoalies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
        cell.ErnGolies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
    }else{
        cell.Task_Start_Date.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
        cell.Task_End_Date.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"endDate"] objectAtIndex:indexPath.row]];
        cell.User_Task.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"event"] objectAtIndex:indexPath.row]];
        cell.TotalGoalies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
        cell.ErnGolies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
    }
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"Delete %@ Row At Index",[[ListData valueForKey:@"id"] objectAtIndex:indexPath.row]);
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus != NotReachable){
         MBProgressHUD *hudDeleteing = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hudDeleteing.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hudDeleteing.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hudDeleteing.contentColor = [UIColor whiteColor];
            hudDeleteing.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        if (searchEnabled){
        dictParam = @{@"id":[[searchResults valueForKey:@"id"] objectAtIndex:indexPath.row]};
        NSLog(@"Dic %@",dictParam);
        }else{
            dictParam = @{@"id":[[ListData valueForKey:@"id"] objectAtIndex:indexPath.row]};
            NSLog(@"Dic %@",dictParam);
        }
        NSString *loginURL=[NSString stringWithFormat:@"%@delete-myEvents",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                dispatch_async(dispatch_get_main_queue(), ^{
                [self loadingElement];
                [hudDeleteing hideAnimated:YES];
                });
            }else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hudDeleteing hideAnimated:YES ];
                });
                [self alertWithTitle:kAppNameAlert message:@"User name or password is incorrect" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }else if (response==NULL){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hudDeleteing hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hudDeleteing hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hudDeleteing hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
        }
    }
}
-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    if (searchEnabled){
        MyGoalRewardViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
        controler.Indexarray=[searchResults objectAtIndex:indexPath.row];
        controler.TaskID = [[searchResults valueForKey:@"eventId"] objectAtIndex:indexPath.row];
        controler.isEditEndDate = _isPendingTask;
        [self.navigationController pushViewController:controler animated:YES];
    }else{
        MyGoalRewardViewController *controler = [self.storyboard instantiateViewControllerWithIdentifier:@"MyGoalRewardViewController"];
        controler.Indexarray=[ListData objectAtIndex:indexPath.row];
        controler.TaskID = [[ListData valueForKey:@"eventId"] objectAtIndex:indexPath.row];
        controler.isEditEndDate = _isPendingTask;
        [self.navigationController pushViewController:controler animated:YES];
    }
}
//EditMeTaskIdentifier
#pragma mark  PrepareForSegue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath * indexPath = self.TableView.indexPathForSelectedRow;
    NSLog(@"Tag Index %ld",(long)indexPath.row);
    
//    if ([segue.identifier isEqualToString:@"EditMeTaskIdentifier"])
//    {
//        MeGoalCreateViewController *countryPicker = segue.destinationViewController;
//        countryPicker.EditMeTaskCheck=@"EditMeTask";
//    }
}
-(void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    if (airlineName == nil) {
        // If empty the search results are the same as the original data
        searchResults = [ListData mutableCopy];
    } else {
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }else if ([scope isEqualToString:@"1"]){
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchBar.text.length == 0){
        searchEnabled = NO;
        [self.TableView reloadData];
    }else{
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text.length==0){
        [searchBar resignFirstResponder];
    }else{
        [searchBar resignFirstResponder];
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.TableView reloadData];
}
-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

@end
