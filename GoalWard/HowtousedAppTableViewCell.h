//
//  HowtousedAppTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 10/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowtousedAppTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *WelComeTititle;
@property (strong, nonatomic) IBOutlet UILabel *DescriptionTitile;

@end
