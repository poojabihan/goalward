//
//  WeGuideLineCellTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeGuideLineCellTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *GuideLineText;

@property (strong, nonatomic) IBOutlet UIButton *Delete_Icon;




@end
