//
//  ViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface ViewController : UIViewController


@property (strong, nonatomic) IBOutlet ACFloatingTextField *UserNameTF;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *PasswordTF;

- (IBAction)Login_Submit_button:(id)sender;
- (IBAction)Fb_login_button:(id)sender;

//-------  Button -- Outlet------

@property (strong, nonatomic) IBOutlet UIButton *ForgotButtonInstance;

@property (strong, nonatomic) IBOutlet UIButton *LoginButtonInstance;

@property (strong, nonatomic) IBOutlet UIButton *SignUpButtonInstance;

@property (strong, nonatomic) IBOutlet UIButton *LoginWithFbButtonInstance;



@end

