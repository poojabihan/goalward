//
//  ForgotPasswordViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
@interface ForgotPasswordViewController ()
{
     MBProgressHUD * hud;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.SubmitButtonInstance setExclusiveTouch:YES];
    [self.SubmitButtonInstance.layer setMasksToBounds:YES];
    [self.SubmitButtonInstance.layer setCornerRadius:5.0f];
    //self.SubmitButtonInstance.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark  UITextfield Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
- (IBAction)SUbmit_button:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        
        if(([self.ForgotPasswordTF.text isEqualToString:@""]))
        {
            
            [self validationErrorAlert:@"Please, Fill all the details."];
            
        }
        
        else if(![self NSStringIsValidEmail:self.ForgotPasswordTF.text])
        {
            
            [self validationErrorAlert:@"Invalid e-mail address!"];
            
        }
        
        else
        {
            
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.contentColor = khudColour;
            
            // Set the label text.
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSDictionary *dictParam = @{@"email":self.ForgotPasswordTF.text};
            
            NSString *loginURL=[NSString stringWithFormat:@"%@forgotPassword",KBaseUrl];
            
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                
                NSLog(@"response:%@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [hud hideAnimated:YES ];
                        NSLog(@"Response Success");
                    });
                    
                    [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"Please try again later.s" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                          [hud hideAnimated:YES ];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                           [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                
            } failure:^(NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                      [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
            
        }
    }
    else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    
    
}
@end
