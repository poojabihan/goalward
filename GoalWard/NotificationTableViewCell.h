//
//  NotificationTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 29/12/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *UserProfile;

@property (strong, nonatomic) IBOutlet UILabel *NotificationText;

@property (strong, nonatomic) IBOutlet UILabel *DateAndTime;

@end
