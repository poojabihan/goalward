//
//  FriendListViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FCAlertView.h>
@interface FriendListViewController : UIViewController



- (IBAction)inviteFriend:(id)sender;

- (IBAction)CurrentTaskList:(id)sender;

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView;

@end
