//
//  CommentViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface CommentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *ContainView;
@property (strong, nonatomic) IBOutlet UITableView *TableView;
- (IBAction)SendComment:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet UIView *ShowTaskHistoryView;
@property (strong, nonatomic) IBOutlet UIView *ShowAllCommentView;
@property (strong, nonatomic) IBOutlet UIView *TbBackGroundView;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *CommentTextField;
@property (strong, nonatomic) IBOutlet UILabel *EarnGoaliesLabel;
@property (strong, nonatomic) IBOutlet UILabel *TotalGoaliesLabel;
@property (strong, nonatomic) IBOutlet UIView *EarnGoliesView;
@property (strong, nonatomic) IBOutlet UIView *TotalGoaliesView;
@property (strong, nonatomic) IBOutlet UIProgressView *ProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *TaskEndDate;
@property (strong, nonatomic) IBOutlet UILabel *TaskStartDate;
@property (strong, nonatomic) IBOutlet UILabel *TaskTitle;

@property NSString *EventID;


@end
