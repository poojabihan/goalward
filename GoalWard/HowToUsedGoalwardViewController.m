//
//  HowToUsedGoalwardViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 10/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "HowToUsedGoalwardViewController.h"
#import "HowtousedAppTableViewCell.h"
@interface HowToUsedGoalwardViewController ()
{
    NSMutableArray *title;NSMutableArray *desc;
}
@end

@implementation HowToUsedGoalwardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableview.estimatedRowHeight=_tableview.rowHeight;
    self.tableview.rowHeight=UITableViewAutomaticDimension;
    self.ScrollView.contentSize = CGSizeMake(0, 1000);
    title = [[NSMutableArray alloc] init];
    desc = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [title addObject:@"Welcome to Goalward"];
        [title addObject:@"Create Goal"];
        [title addObject:@"Create Me Goal & We Goal"];
        [title addObject:@"Goalies Assigned"];
        [title addObject:@"Invite Friends"];
        [title addObject:@"Leaderboard"];
        [title addObject:@"Social World"];
       
        [desc addObject:@"Hello and welcome to Goalward! The Goal setting App made by me, Goalie. I want you to reach your full potential in life! Here's how you use my App to help you achieve your goals."];
        [desc addObject:@"A Me Goal is a goal for one person.\nA We Goal is a goal for a group of people."];
        [desc addObject:@"Create your goal by giving it a name in the add task section. Describe what you need to do to complete your goal in the guidelines. If it is a We Goal, invite your friends to participate."];
        [desc addObject:@"Based on the duration you've set for you goal, I will assign you a number of Goalies you'll need to earn to complete your goal. You earn 1 goalie for each time you successfully complete one of your goal's guidelines."];
        [desc addObject:@"For either a Me Goal, or a We Goal, you can invite people to participate in your progress. Invite your Grandparent, Manager, Teacher or Turtle to the Goalward App and connect with them. They can now give you the high five's you need to complete your goal!"];
        [desc addObject:@"As you add friend's within the app, you can view the leaderboard of everyone using the app to see their progress on their own goals and cheer them on."];
        [desc addObject:@"Success! Once you've completed your goal let the Social World hear it with a post!"];
    
        [_tableview reloadData];
    });
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [title count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    HowtousedAppTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HowtousedAppTableViewCell"];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(cell == nil){
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HowtousedAppTableViewCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.DescriptionTitile.text = [desc objectAtIndex:indexPath.row];
    
    cell.WelComeTititle.text=[title objectAtIndex:indexPath.row];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
@end
