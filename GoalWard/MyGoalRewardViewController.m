//
//  MyGoalRewardViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 01/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "MyGoalRewardViewController.h"
#import "RatingView.h"
#import "Webservice.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "GuideLineRatingTableViewCell.h"
#import "UIColor+Hexadecimal.h"
#import <FCAlertView.h>
#import "CommentHistoryViewController.h"
#import "TaskListViewController.h"
#import "LGAlertView.h"
#import "CustomeView.h"
@interface MyGoalRewardViewController ()<RatingViewDelegate,FCAlertViewDelegate,LGAlertViewDelegate,UITextFieldDelegate>
{
    MBProgressHUD *hud;float RatingValue;NSMutableArray *GuideLine;RatingView *RV0;
    NSMutableArray *GuidelineIds;  UIColor *color;NSString *subtitleColor;
    NSMutableArray *ListData;
    UIDatePicker *picker;
    UIToolbar* toolbar;
    NSString *strEndDate;
    NSMutableArray *allUsersArr;
}
@property NSDateFormatter *dateFormat;
@property NSDateFormatter *Dateformate2;
@property NSMutableArray * selectedArray;
@property (retain, nonatomic) UIImage *alertImage;
@property (retain, nonatomic) NSString *alertTitle;
@property (retain, nonatomic) NSArray *arrayOfButtonTitles;
@end

@implementation MyGoalRewardViewController
@synthesize TaskID;
- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self) {
        self.Dateformate2 = [[NSDateFormatter alloc] init];
        [self.Dateformate2 setDateFormat:@"MM/dd/YYYY"];
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    GuideLine = [[NSMutableArray alloc] init];
    GuidelineIds = [[NSMutableArray alloc] init];
    ListData = [[NSMutableArray alloc] init];
    self.ContainViewColorClear.backgroundColor=[UIColor clearColor];
    
    self.GuidlineLabel.layer.borderWidth=1;
    self.GuidlineLabel.layer.borderColor=[UIColor whiteColor].CGColor;
    self.GuidlineLabel.layer.cornerRadius=5;
    
    self.EarnGoaliesView.backgroundColor=[UIColor clearColor];
    self.EarnGoaliesView.layer.borderWidth=1;
    self.EarnGoaliesView.layer.borderColor=[UIColor colorWithHex:@"12B7DD"].CGColor;
    self.EarnGoaliesView.layer.masksToBounds=YES;
    self.TotalGoaliesView.backgroundColor=[UIColor clearColor];
    self.TotalGoaliesView.layer.borderWidth=1;
    self.TotalGoaliesView.layer.borderColor=[UIColor colorWithHex:@"12B7DD"].CGColor;
    self.TotalGoaliesView.layer.masksToBounds=YES;
    
    self.ShowTaskHistoryView.backgroundColor=[UIColor clearColor];
    self.ShoeCommentHistoryView.backgroundColor=[UIColor clearColor];
    [self.ScrollView setScrollEnabled:YES];
    self.ProgressSlider.layer.cornerRadius=5;
    self.ProgressSlider.layer.masksToBounds=YES;
    self.BAckGroundViewYableview.backgroundColor=[UIColor clearColor];
    if ([self.selectedArray count]==0) {
        self.selectedArray = [[NSMutableArray alloc]init];
    }
    self.TableView.backgroundColor=[UIColor clearColor];
    //_TableView.estimatedRowHeight=_TableView.rowHeight;
    //_TableView.rowHeight=UITableViewAutomaticDimension;
    [self.SubmitButtonInstance setExclusiveTouch:YES];
    [self.SubmitButtonInstance.layer setMasksToBounds:YES];
    [self.SubmitButtonInstance.layer setCornerRadius:20.0f];
    self.SubmitButtonInstance.backgroundColor = [UIColor colorWithHex:@"3ac3f9"];
    [self UserDataLoading];
    
    if (_isEditEndDate) {
        self.EndDateLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *endDateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endDateTapAction:)];
        [self.EndDateLabel addGestureRecognizer:endDateTap];
        self.awardGoalieView.alpha = 0.4;
        self.removeGoalieView.alpha = 0.4;
    }
    
    self.CommentTextField.layer.borderWidth=1.0;
    self.CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;

    allUsersArr = [[NSMutableArray alloc] init];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)endDateTapAction:(UITapGestureRecognizer*)sender {
    picker = [[UIDatePicker alloc] init];
    picker.backgroundColor = [UIColor whiteColor];
    [picker setValue:[UIColor blackColor] forKey:@"textColor"];
    
    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    picker.datePickerMode = UIDatePickerModeDateAndTime;
    picker.minimumDate = [NSDate date];
    
    NSString *dateString =  [_EndDateLabel.text stringByReplacingOccurrencesOfString:@"End Date: " withString:@""];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-dd-MM - HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    if (date == nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy - HH:mm:ss"];
        date = [dateFormat dateFromString:dateString];
    }
    
    if (date == nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd - HH:mm:ss"];
        date = [dateFormat dateFromString:dateString];
    }
    [picker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    picker.backgroundColor=[UIColor colorWithHex:@"2E3992"];
    picker.date = date;
    strEndDate = dateString;

    [picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    picker.frame = CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 300);
    [self.view addSubview:picker];
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 50)];
//    toolbar.barStyle = UIBarStyleBlackTranslucent;
    [toolbar setTintColor:[UIColor whiteColor]];
//    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDoneButtonClick)]];
    
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDoneButtonClick)];
    [doneBtn setTintColor:[UIColor colorWithHex:@"2E3992"]];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];

    
    [toolbar sizeToFit];
    [self.view addSubview:toolbar];
}

-(void) dueDateChanged:(UIDatePicker *)sender {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy - HH:mm:ss"];

    NSDateFormatter* dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-dd-MM - HH:mm:ss"];

    NSLog(@"Picked the date %@", [dateFormatter stringFromDate:[sender date]]);
    _EndDateLabel.text = [NSString stringWithFormat:@"End Date: %@",[dateFormatter2 stringFromDate:[sender date]]];
    strEndDate = [dateFormatter1 stringFromDate:[sender date]];
}

-(void)onDoneButtonClick {
    [toolbar removeFromSuperview];
    [picker removeFromSuperview];
    
    [self updateEndDate];
}

-(void)updateEndDate {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam = @{@"eventId":TaskID,@"endDate":strEndDate};
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@updateDate",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            [hud hideAnimated:YES ];

            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:nil message:[response valueForKey:@"message"] actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)UserDataLoading{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSString  *CurrentDate =  [self.Dateformate2 stringFromDate:[NSDate date]];
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSLog(@"Task Id %@  -  current Date %@",TaskID,CurrentDate);
        NSDictionary *dictParam = @{@"eventId":TaskID,@"date":CurrentDate};
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@showMyEvent",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];

                ListData = [response valueForKey:@"Data"];
                self.GuidlineLabel.text=[NSString stringWithFormat:@"  %@",[[response valueForKey:@"Data"]valueForKey:@"guideline"]];
                self.TaskListLbl.text=[[response valueForKey:@"Data"] valueForKey:@"event"];
                self.StartDateLbl.text=[NSString stringWithFormat:@"Start Date: %@ - %@",[[response valueForKey:@"Data"] valueForKey:@"startDate"],[[response valueForKey:@"Data"] valueForKey:@"startTime"]];
                self.EndDateLabel.text=[NSString stringWithFormat:@"End Date: %@ - %@",[[response valueForKey:@"Data"] valueForKey:@"endDate"],[[response valueForKey:@"Data"] valueForKey:@"endTime"]];
                GuideLine = [[response valueForKey:@"Data"] valueForKey:@"guideline"];
                GuidelineIds = [[[response valueForKey:@"Data"] valueForKey:@"guidelineids"]valueForKey:@"id"];
                self.EarnGoalies.text=[NSString stringWithFormat:@"%@",[[response valueForKey:@"Data"] valueForKey:@"earnGoalie"]];
                self.TotalGoalies.text=[NSString stringWithFormat:@"%@",[[response valueForKey:@"Data"] valueForKey:@"goalies"]];
                    allUsersArr = [[[response valueForKey:@"Data"] valueForKey:@"allUser"] mutableCopy];
                    [allUsersArr removeObject:LoginUserId];
                    
                if ([[NSString stringWithFormat:@"%@",[[response valueForKey:@"Data"] valueForKey:@"progress"]] isEqualToString:@"100"]) {
                    
                    NSString*progressValue=@"100";
                    NSLog(@"%@",progressValue);
                    float Pro = [progressValue floatValue];
                    self.ProgressSlider.progress=Pro;
                    
                    self.ProgressLbl.text=@"100%";
                    
                }else{
                    
                NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[response valueForKey:@"Data"] valueForKey:@"progress"]];
                NSLog(@"%@",progressValue);
                float Pro = [progressValue floatValue];
                self.ProgressSlider.progress=Pro;
                self.ProgressLbl.text=[NSString stringWithFormat:@"%@%%",[[response valueForKey:@"Data"] valueForKey:@"progress"]];
                }
                    
//                for (int ch = 0; ch <[GuidelineIds count]; ch++) {
//                    if ([[NSString stringWithFormat:@"%@",[[[ListData valueForKey:@"guidelineids"]valueForKey:@"guidelineStatus"]objectAtIndex:ch]] isEqualToString:@"TRUE"]) {
//                            NSString*GetID=[GuidelineIds objectAtIndex:ch];
//                            [_selectedArray addObject: GetID];
//                        }
//                    NSLog(@"Select Array Web Services %@",_selectedArray);
//                }
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [self.TableView reloadData];
                    [hud hideAnimated:YES];
                });
                NSLog(@"Response Success");
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [GuideLine count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    GuideLineRatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GuideLineRatingTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"GuideLineRatingTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.guideTesx.text=[NSString stringWithFormat:@"%@",[[GuideLine valueForKey:@"guideline"] objectAtIndex:indexPath.row]];
    if ([self.selectedArray containsObject:[GuidelineIds objectAtIndex:  indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
       // cell.checkImage.image=[UIImage imageNamed:@"GuidlineCheck"];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        //cell.checkImage.image=[UIImage imageNamed:@"guidlineUncheck"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    //GuideLineRatingTableViewCell*cellCheck = [[GuideLineRatingTableViewCell alloc] init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.selectedArray addObject:[GuidelineIds objectAtIndex:indexPath.row]];
            NSLog(@"Search Select Array %@",self.selectedArray);
            [self.TableView reloadData];
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.selectedArray removeObject:[GuidelineIds objectAtIndex:  indexPath.row]];
            NSLog(@"Search Select Array After Delete %@",self.selectedArray);
            [self.TableView reloadData];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (IBAction)SubmitButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        if ([_selectedArray count]==0) {
            [self validationErrorAlert:@"Select Guideline"];
        }
        else
        {
        //NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSString *TodayDate =  [self.dateFormat stringFromDate:[NSDate date]];
        NSString *GuidelineIds = [_selectedArray componentsJoinedByString:@","];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam = @{@"event_id":self.TaskID,@"date":TodayDate,@"guidelineids":GuidelineIds};
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@complete-guideline",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
            NSString *SubTitle=[NSString stringWithFormat:@"Thanks Your Submission.😊"];
                self.alertImage = [UIImage imageNamed:@"GoliesIcon"];
                [self showAlertInView:self withTitle:nil withSubtitle:SubTitle withCustomImage:self.alertImage withDoneButtonTitle:@"Done" andButtons:nil];
            
                NSLog(@"Response Success");
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:kAppNameAlert message:@"Submission Already Completed.." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
- (void) showAlertInView:(UIViewController *)view withTitle:(NSString *)title withSubtitle:(NSString *)subTitle withCustomImage:(UIImage *)image withDoneButtonTitle:(NSString *)done andButtons:(NSArray *)buttons {
    FCAlertView *alert = [[FCAlertView alloc] init];
    self.alertTitle = nil;
    alert.subTitleColor = [self checkFlatColors:subtitleColor];
    alert.blurBackground = 0;
    alert.bounceAnimations = 1;
    alert.darkTheme = NO;
    alert.fullCircleCustomImage = YES;
    alert.delegate = self;
    //[alert makeAlertTypeSuccess];
    alert.avoidCustomImageTint = 1;
    alert.subtitleFont = [UIFont fontWithName:@"Avenir" size:17.0];
    self.arrayOfButtonTitles = @[];
    [alert showAlertInView:view withTitle:title withSubtitle:subTitle withCustomImage:image withDoneButtonTitle:done andButtons:buttons];
}
- (UIColor *) checkFlatColors:(NSString *)selectedColor {
    FCAlertView *alert = [[FCAlertView alloc] init];
    color = alert.flatTurquoise;
    return color;
}
- (IBAction)AwardGoalie:(id)sender {
    if (_isEditEndDate) {
        return;
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSString *TodayDate =  [self.dateFormat stringFromDate:[NSDate date]];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam = @{@"eventId":self.TaskID,@"currentDateTime":TodayDate,@"userId":UserId};
        // eventId,currentDateTime,userId
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@giveReward",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"Getting Response - %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                
                if ([[NSString stringWithFormat:@"%@",[response objectForKey:@"check"]] isEqualToString:@"1"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self UserDataLoading];
                    });
                    
                    [[[LGAlertView alloc]initWithViewAndTitle:@"GoalWard" message:@"Congratulations! You’ve successfully completed your goal!" style:LGAlertViewStyleAlert view:[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil].lastObject
                        buttonTitles:@[@"Submit"] cancelButtonTitle:nil destructiveButtonTitle:nil
                        delegate:self] showAnimated:YES completionHandler:nil];
                    
                    NSLog(@"Animation");
                
                //-------- check = 1 :  Last Date -----------
                
                }else if([[NSString stringWithFormat:@"%@",[response valueForKey:@"check"]]isEqualToString:@"2"]){
                
                   
                //- - - - - -  - -  -- Check = 2 : Submit - - - - -  -
                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self UserDataLoading];
                                    });
            
                                    NSString *SubTitle=[NSString stringWithFormat:@"Nice Job!"];
                                    self.alertImage = [UIImage imageNamed:@"GoliesIcon"];
                                    [self showAlertInView:self withTitle:nil withSubtitle:SubTitle withCustomImage:self.alertImage withDoneButtonTitle:@"Done" andButtons:nil];
                
                }else if ([[NSString stringWithFormat:@"%@",[response valueForKey:@"check"]]isEqualToString:@"3"]){
                    
                // - - -  - -  - -- -Already Given - - - - - - - - -
                
                //[self alertWithTitle:kAppNameAlert message:@"Goalie already earned today." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleDefault];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Goalie already earned today." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok =[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction* nonnull){
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert addAction:ok];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:kAppNameAlert message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
            
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (void)alertViewCancelled:(LGAlertView *)alertView {
    NSLog(@"cancel");
    
}
- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSUserDefaults *store=[NSUserDefaults standardUserDefaults];
    //Buton Index For Multipale Button : -
    NSLog(@"Store Value Check %@",[store valueForKey:@"Rating"]);
    
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        //Web Service:: api/giveRating
       // Keys :: eventId,rating
        NSString *RatingID =[[NSUserDefaults standardUserDefaults]valueForKey:@"Rating"];
        NSDictionary *dictParam = @{@"eventId":self.TaskID,@"rating":RatingID};
        NSLog(@"Dic %@",dictParam);
         NSString *loginURL=[NSString stringWithFormat:@"%@giveRating",KBaseUrl];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Rating Response - %@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *ratingRemove =@"1";
                    NSString *rat = [NSString stringWithFormat:@"%@",ratingRemove];
                    [[NSUserDefaults standardUserDefaults] setObject:rat forKey:@"Rating"];
                    [hud hideAnimated:YES];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Share Your Task!" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *Cancle =[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction* nonnull){
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    UIAlertAction *Share =[UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction* nonnull){
                        
                        NSString *shareWithStringContent = @"Oh, hi! I just crushed my latest goal using the goal setting app GoalWard. Join me in my goal setting quest!  \n https://itunes.apple.com/us/app/goalward/id1333980275?mt=8&ign-mpt=uo%3D4";
                        NSArray *itemsToShare = @[shareWithStringContent];
                        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
                        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypePostToTencentWeibo];
                        [self presentViewController:activityVC animated:YES completion:nil];
                        
                    }];
                    [alert addAction:Cancle];
                    [alert addAction:Share];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                });
            }else{
                 [hud hideAnimated:YES];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                 [hud hideAnimated:YES];
                [self.navigationController popViewControllerAnimated:YES];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (IBAction)RemoveGoalie:(id)sender {
    
    if (_isEditEndDate) {
        return;
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        //NSString *TodayDate =  [self.dateFormat stringFromDate:[NSDate date]];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam = @{@"eventId":self.TaskID,@"userId":UserId};
        NSLog(@"Dic %@",dictParam);
        //Keys :: eventId,userId
        NSString *loginURL=[NSString stringWithFormat:@"%@deleteReward",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"Getting Response - %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self UserDataLoading];
                });
                
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:kAppNameAlert message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)FCAlertDoneButtonClicked:(FCAlertView *)alertView{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Comment"]){
        CommentHistoryViewController * candRoleVC = [segue destinationViewController];
        candRoleVC.EventID=TaskID;
        candRoleVC.allUsersArr = allUsersArr;
    }if ([[segue identifier]isEqualToString:@"ShowTaskHistory"]) {
        TaskListViewController *VC = [segue destinationViewController];
        VC.EventID=TaskID;
    }
}

- (void)keyboardWasShown:(NSNotification*)notification{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
-(void)keyboardWillBeHidden:(NSNotification*)notification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
#pragma mark  UITextfield Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.CommentTextField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;
    return YES;
}

- (IBAction)SendComment:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        if (_CommentTextField.text.length==0) {
            _CommentTextField.layer.borderColor=[UIColor redColor].CGColor;
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSString *UserId=[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            NSDictionary *dictParam = @{@"eventId":TaskID,@"userId":UserId,@"comment":_CommentTextField.text,@"allUser":allUsersArr};
            NSLog(@"Dic %@",dictParam);
            NSString *loginURL=[NSString stringWithFormat:@"%@giveComment",KBaseUrl];
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"Response----- %@",response);
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _CommentTextField.text=@"";
                        [hud hideAnimated:YES ];
                    });
                }else{
                    NSLog(@"Failed");
                    [hud hideAnimated:YES ];
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

@end
