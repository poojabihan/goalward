//
//  ForgotPasswordViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface ForgotPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet ACFloatingTextField *ForgotPasswordTF;

@property (strong, nonatomic) IBOutlet UIButton *SubmitButtonInstance;

- (IBAction)SUbmit_button:(id)sender;






@end
