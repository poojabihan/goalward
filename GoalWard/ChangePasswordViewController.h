//
//  ChangePasswordViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 02/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface ChangePasswordViewController : UIViewController


@property (strong, nonatomic) IBOutlet ACFloatingTextField *CurrentPassword;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *NewPassword;


@property (strong, nonatomic) IBOutlet ACFloatingTextField *ConformPassword;
@property (strong, nonatomic) IBOutlet UIButton *SubmitButtonInstance;
- (IBAction)SubmitButton:(id)sender;

@end
