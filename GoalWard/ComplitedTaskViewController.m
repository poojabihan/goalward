//
//  ComplitedTaskViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 03/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "ComplitedTaskViewController.h"
#import "CompliteTaskTableViewCell.h"
#import "Webservice.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "RatingView.h"
#import "UIColor+Hexadecimal.h"
@interface ComplitedTaskViewController ()<RatingViewDelegate>
{
    MBProgressHUD *hud;NSMutableArray *ListData;NSArray *searchResults;
    BOOL searchEnabled;NSDictionary *dictParam;
}
@property NSDateFormatter *dateFormat;
@property (strong, nonatomic) RatingView *ratingView;
@end

@implementation ComplitedTaskViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.SearchBar setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    [self.SearchBar setBarTintColor:[UIColor colorWithHex:@"0b122f"]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    self.TableView.backgroundColor=[UIColor clearColor];
    [self servicesLoading];
    self.TableView.tableFooterView = [UIView new];

    
}
-(void)servicesLoading{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        NSString  *CurrentDate =  [self.dateFormat stringFromDate:[NSDate date]];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":UserId,@"currentDateTime":CurrentDate};
        NSLog(@"Dic %@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@getCompleteEvents",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            
            NSLog(@"response:%@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1))
            {
                
                ListData = [response valueForKey:@"Data"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[_TableView reloadData];
                     [_TableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [hud hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    _TableView.hidden=YES;
                }else{
                    _TableView.hidden=NO;
                    
                }
                
            }
            else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
            } failure:^(NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            
            }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        return [searchResults count];
    }else{
        return [ListData count];
    }
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    CompliteTaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompliteTaskTableViewCell"];
    //cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backview.backgroundColor=[UIColor clearColor];
    [cell.backview.layer setCornerRadius:5.0f];
    [cell.backview.layer setBorderColor:[UIColor clearColor].CGColor];
    [cell.backview.layer setBorderWidth:0.2f];
    [cell.backview.layer setShadowColor:[UIColor clearColor].CGColor];
    [cell.backview.layer setShadowOpacity:5.0];
    [cell.backview.layer setShadowRadius:5.0];
    [cell.backview.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    cell.RatingView.backgroundColor=[UIColor clearColor];
    cell.ProgressBar.layer.cornerRadius=5.0;
    cell.ProgressBar.layer.masksToBounds=YES;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"CompliteTaskTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    self.ratingView = [[RatingView alloc] initWithFrame:CGRectMake(80, 0, 200, 45)
                                      selectedImageName:@"selected.png"
                                        unSelectedImage:@"unSelected.png"
                                               minValue:0
                                               maxValue:5
                                          intervalValue:0.5
                                             stepByStep:NO];
    self.ratingView.delegate = self;
    self.ratingView.gestureRecognizers=NO;
    if (searchEnabled) {
        cell.TaskStartDate.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
        cell.TaskEndDate.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"endDate"] objectAtIndex:indexPath.row]];
        cell.TaskDetails.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"event"] objectAtIndex:indexPath.row]];
        cell.totalgoalies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
        cell.erngolies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
        cell.progressLbl.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        
        NSString *Rat = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        
        if ([Rat isEqualToString:@"100"]) {
            
            NSString*progressValue=@"100";
            //NSLog(@"%@",progressValue);
            float Pro = [progressValue floatValue];
            cell.ProgressBar.progress=Pro;
            cell.progressLbl.text=@"100%";
            
        }else{
            
            NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[searchResults valueForKey:@"progress"]objectAtIndex:indexPath.row]];
            //NSLog(@"%@",progressValue);
            float Pro = [progressValue floatValue];
            cell.ProgressBar.progress=Pro;
            cell.progressLbl.text=[NSString stringWithFormat:@"%@%%",[[searchResults valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        }
        NSString *rat = [[searchResults valueForKey:@"rating"] objectAtIndex:indexPath.row];
        [cell.RatingView addSubview:self.ratingView];
        
        self.ratingView.value=rat.floatValue;
        cell.ShareButtonClick.tag = indexPath.row;
        [cell.ShareButtonClick addTarget:self action:@selector(ShareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.TaskStartDate.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
        cell.TaskEndDate.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"endDate"] objectAtIndex:indexPath.row]];
        cell.TaskDetails.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"event"] objectAtIndex:indexPath.row]];
        cell.totalgoalies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
        cell.erngolies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
        cell.progressLbl.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        
        NSString *rat = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        
        if ([rat isEqualToString:@"100"]) {
            
            NSString*progressValue=@"100";
            //NSLog(@"%@",progressValue);
            float Pro = [progressValue floatValue];
            cell.ProgressBar.progress=Pro;
            cell.progressLbl.text=@"100%";
            
        }else{
            
            NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
            //NSLog(@"%@",progressValue);
            float Pro = [progressValue floatValue];
            cell.ProgressBar.progress=Pro;
            cell.progressLbl.text=[NSString stringWithFormat:@"%@%%",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
        }
        NSString *rat2 = [[ListData valueForKey:@"rating"] objectAtIndex:indexPath.row];
        [cell.RatingView addSubview:self.ratingView];
        self.ratingView.value=rat2.floatValue;
        cell.ShareButtonClick.tag = indexPath.row;
        [cell.ShareButtonClick addTarget:self action:@selector(ShareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

-(void)ShareButtonClick:(UIButton*)sender{
    
    NSString *shareWithStringContent = @"Oh, hi! I just crushed my latest goal using the goal setting app GoalWard. Join me in my goal setting quest!  \n https://itunes.apple.com/us/app/goalward/id1333980275?mt=8&ign-mpt=uo%3D4";
    NSArray *itemsToShare = @[shareWithStringContent];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypePostToTencentWeibo];
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        
        if (internetStatus != NotReachable)
        {
            
            MBProgressHUD *hudDeleteing = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hudDeleteing.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hudDeleteing.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hudDeleteing.contentColor = [UIColor whiteColor];
            hudDeleteing.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            if (searchEnabled) {
                dictParam = @{@"id":[[searchResults valueForKey:@"id"] objectAtIndex:indexPath.row]};
                
                NSLog(@"Dic %@",dictParam);
            }else{
                dictParam = @{@"id":[[ListData valueForKey:@"id"] objectAtIndex:indexPath.row]};
                NSLog(@"Dic %@",dictParam);
            }
            NSString *loginURL=[NSString stringWithFormat:@"%@delete-myEvents",KBaseUrl];
            
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                
                NSLog(@"response:%@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self servicesLoading];
                        [hudDeleteing hideAnimated:YES];
                    });
                }
                else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hudDeleteing hideAnimated:YES ];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"User name or password is incorrect" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }
                else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hudDeleteing hideAnimated:YES ];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hudDeleteing hideAnimated:YES];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
            } failure:^(NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hudDeleteing hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
            
        }else{
            [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
        }
        
    }
}
- (void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    
    if (airlineName == nil) {
        
        // If empty the search results are the same as the original data
        searchResults = [ListData mutableCopy];
        
    } else {
        
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
            
        }
        else if ([scope isEqualToString:@"1"]){
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.TableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length==0) {
        [searchBar resignFirstResponder];
    }else{
        [searchBar resignFirstResponder];
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.TableView reloadData];
    
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
