//
//  SettingTableViewCell.m
//  GoalWard
//
//  Created by Alok Mishra on 31/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "SettingTableViewCell.h"

@implementation SettingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
