//
//  MyGoalRewardViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 01/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FCAlertView.h>
#import "ACFloatingTextField.h"

@interface MyGoalRewardViewController : UIViewController

@property BOOL isEditEndDate;
@property (strong, nonatomic) NSString *TaskID;
@property NSArray *Indexarray;
@property (strong, nonatomic) IBOutlet UIView *RatingView;
@property (strong, nonatomic) IBOutlet UIView *BAckGroundViewYableview;
@property (strong, nonatomic) IBOutlet UIView *awardGoalieView;
@property (strong, nonatomic) IBOutlet UIView *removeGoalieView;

@property (strong, nonatomic) IBOutlet UIView *ContainViewColorClear;

@property (strong, nonatomic) IBOutlet UILabel *TaskListLbl;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet UIView *EarnGoaliesView;
@property (strong, nonatomic) IBOutlet UIView *TotalGoaliesView;
@property (strong, nonatomic) IBOutlet UIView *ShowTaskHistoryView;
@property (strong, nonatomic) IBOutlet UIView *ShoeCommentHistoryView;

@property (strong, nonatomic) IBOutlet UITableView *TableView;

@property (strong, nonatomic) IBOutlet UILabel *StartDateLbl;

@property (strong, nonatomic) IBOutlet UILabel *EndDateLabel;

@property (strong, nonatomic) IBOutlet UILabel *GuidlineLabel;
@property (strong, nonatomic) IBOutlet UIButton *SubmitButtonInstance;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *CommentTextField;

- (IBAction)SubmitButtonClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIProgressView *ProgressSlider;
@property (strong, nonatomic) IBOutlet UILabel *EarnGoalies;
@property (strong, nonatomic) IBOutlet UILabel *TotalGoalies;
@property (strong, nonatomic) IBOutlet UILabel *ProgressLbl;
- (IBAction)AwardGoalie:(id)sender;


- (IBAction)RemoveGoalie:(id)sender;


- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView;
@end
