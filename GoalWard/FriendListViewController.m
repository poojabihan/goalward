//
//  FriendListViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "FriendListViewController.h"
#import "TaskListMeGoalViewController.h"
#import <FCAlertView.h>
@interface FriendListViewController ()<FCAlertViewDelegate>
{
    UIColor *color;NSString *subtitleColor;
}
@property (retain, nonatomic) UIImage *alertImage;
@property (retain, nonatomic) NSString *alertTitle;
@property (retain, nonatomic) NSArray *arrayOfButtonTitles;
@end
@implementation FriendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}
- (IBAction)inviteFriend:(id)sender{
  //  blank_icon.png
  //  self.alertImage = [UIImage imageNamed:@"GoliesIcon"];
  //  [self showAlertInView:self withTitle:nil withSubtitle:@"👍🏻This is my alert's subtitle. Keep it short and concise. 😜 " withCustomImage:self.alertImage withDoneButtonTitle:@"Done" andButtons:nil];
//    [alert showAlertInView:self
//                 withTitle:self.alertTitle
//              withSubtitle:@"This is my alert's subtitle.Keep it short and concise. 😜"
//           withCustomImage:_alertImage
//       withDoneButtonTitle:@"Done"
//                andButtons:self.arrayOfButtonTitles];
}

-(void)showAlertInView:(UIViewController *)view withTitle:(NSString *)title withSubtitle:(NSString *)subTitle withCustomImage:(UIImage *)image withDoneButtonTitle:(NSString *)done andButtons:(NSArray *)buttons {
    FCAlertView *alert = [[FCAlertView alloc] init];self.alertTitle = nil;
    alert.blurBackground = 1;alert.darkTheme = YES;alert.delegate = self;
    alert.subTitleColor = [self checkFlatColors:subtitleColor];
    alert.avoidCustomImageTint = 1;alert.bounceAnimations = 1;
    alert.fullCircleCustomImage = YES;self.arrayOfButtonTitles = @[];
    //[alert makeAlertTypeSuccess];
    [alert showAlertInView:view withTitle:title withSubtitle:subTitle withCustomImage:image withDoneButtonTitle:done andButtons:buttons];
}

-(UIColor *)checkFlatColors:(NSString *)selectedColor{
    FCAlertView *alert = [[FCAlertView alloc] init];
    color = alert.flatTurquoise;
    return color;
}

-(void)FCAlertDoneButtonClicked:(FCAlertView *)alertView{

}
-(IBAction)CurrentTaskList:(id)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pendingTasks"]){
        TaskListMeGoalViewController * list = [segue destinationViewController];
        list.isPendingTask = YES;
    }
}

@end
