//
//  CustomeView.h
//  GifDemo
//
//  Created by Alok Mishra on 08/12/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+animatedGIF.h"
#import "RatingView.h"
@interface CustomeView : UIView<RatingViewDelegate>
{
    float RatingValue;
}
@property (strong, nonatomic) IBOutlet UIImageView *Gotimage;
@property (strong, nonatomic) RatingView *ratingView;
@property (strong, nonatomic) IBOutlet UIView *RatView;
@property (strong, nonatomic) NSString *RatingString;
@end
