//
//  WeGoalPeopleTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeGoalPeopleTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UIImageView *profile_pic;


@property (strong, nonatomic) IBOutlet UILabel *UserName_Lbl;



@end
