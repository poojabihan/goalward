//
//  ChangePasswordViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 02/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "ChangePasswordViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "UIColor+Hexadecimal.h"
@interface ChangePasswordViewController (){
     MBProgressHUD *hud;
}
@end
@implementation ChangePasswordViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.SubmitButtonInstance setExclusiveTouch:YES];
    [self.SubmitButtonInstance.layer setMasksToBounds:YES];
    [self.SubmitButtonInstance.layer setCornerRadius:20.0f];
    self.SubmitButtonInstance.backgroundColor = [UIColor colorWithHex:@"43C3F9"];
    self.title = @"Change Password";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }else {
        [textField resignFirstResponder];
    }
}

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    int duration = 1; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}

#pragma mark  Alert Prompt Method

- (IBAction)SubmitButton:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        
        if(_CurrentPassword.text.length==0){
            
            [self validationErrorAlert:@"Fill Current Password."];
        }else if (_NewPassword.text.length==0){
            
            [self validationErrorAlert:@"Fill New Password."];
        }else if(_ConformPassword.text.length==0){
            
            [self validationErrorAlert:@"Fill Confirm Password."];
        }else if(![_NewPassword.text isEqualToString:_ConformPassword.text]){
            
            [self validationErrorAlert:@"Password Do Not Match"];
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            
            NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            
            NSDictionary *dictParam1 = @{@"userId":UserId,@"password":_CurrentPassword.text,@"newPassword":_NewPassword.text};
            
            NSLog(@"Dic %@",dictParam1);
            
            NSString *loginURL=[NSString stringWithFormat:@"%@changePassword",KBaseUrl];
            
            [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
                
                NSLog(@"response:%@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Your Password Change Successfully." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert addAction:ok];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:true completion:nil];
                    });
                    
                }else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    }
                
                } failure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                }];
            NSLog(@"Loading");
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
@end
