//
//  WeGoalPeopleViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 16/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "WeGoalPeopleViewController.h"
#import "WeGoalPeopleTableViewCell.h"
#import <MBProgressHUD.h>
#import "Reachability.h"
#import <AFNetworking/AFNetworking.h>
#import "Webservice.h"
#import "UIImageView+WebCache.h"
#import "GoalwardUserListViewController.h"
#import "UIColor+Hexadecimal.h"
@interface WeGoalPeopleViewController ()
{
    MBProgressHUD * hud; NSArray *searchResults;
    BOOL searchEnabled;
}
@end

@implementation WeGoalPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.hidesBackButton=YES;
    //090D1C
    [self.search_Bar setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    [self.search_Bar setBarTintColor:[UIColor colorWithHex:@"0b122f"]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    self.tableview.backgroundColor = [UIColor colorWithHex:@"0b122f"];
    self.view.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    self.tableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.tableview.tableFooterView = [UIView new];
    // [_search_Bar setShowsScopeBar: NO];
    [self loadingElementsInCandidatRolesTVC];
    // Do any additional setup after loading the view.
}
-(void)loadingElementsInCandidatRolesTVC{
    self.candidateRoleNamesArray = [[NSMutableArray alloc]init];
    if ([self.selectedArray count]==0) {
        self.selectedArray = [[NSMutableArray alloc]init];
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",LoginUserId);
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":LoginUserId};
        NSLog(@"Dic %@",dictParam1);
        NSString * loginURL=[NSString stringWithFormat:@"%@showFriendList",KBaseUrl];
        NSLog(@"Url Check - %@",loginURL);
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)){
                self.candidateRoleNamesArray = [response objectForKey:@"data"];
                if ([_candidateRoleNamesArray count]==0) {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Record Not Found, Please Add Friend First." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction * logout = [UIAlertAction actionWithTitle:@"All User" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        GoalwardUserListViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalwardUserListViewController"];
                        [self.navigationController pushViewController:vc animated:YES];
                    }];
                    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Invite Friend's" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        NSString *shareWithStringContent = @"Greetings Goal Oriented Friend! I’m using a goal setting app called GoalWard. If you connect with me in the app we can cheer each other on in our quest to complete our personal goals. Who isn’t in need of a fist bump every now and then? 🤜🏻🤛🏼  \n https://itunes.apple.com/us/app/goalward/id1333980275?mt=8&ign-mpt=uo%3D4";
                        NSArray *itemsToShare = @[shareWithStringContent];
                        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
                        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypePostToTencentWeibo];
                        [self presentViewController:activityVC animated:YES completion:nil];
                    }];
                    [alert addAction:logout];
                    [alert addAction:dismiss];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }else{
                    NSLog(@"Multipale Friends");
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.tableview reloadData];
                });
                
            }else if (response==NULL) {

                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
#pragma mark  Alert Method

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchEnabled) {
        return [searchResults count];
    }
    else{
        return [self.candidateRoleNamesArray count];
    }
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    WeGoalPeopleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WeGoalPeopleTableViewCell"];
    cell.profile_pic.layer.cornerRadius = cell.profile_pic.frame.size.width/2;
    cell.profile_pic.layer.masksToBounds = YES;
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(cell==nil){
        
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"WeGoalPeopleTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (searchEnabled) {
            cell.UserName_Lbl.text = [[searchResults objectAtIndex:indexPath.row]objectForKey:@"name"];
        NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
        
        if ([SocialCheck isEqualToString:@"Y"]) {
            
            [cell.profile_pic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
            
        }else{
            
            [cell.profile_pic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[searchResults valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
            
        }
        if ([self.selectedArray containsObject:[[searchResults objectAtIndex:  indexPath.row]objectForKey:@"userId"]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else{
            cell.UserName_Lbl.text = [[_candidateRoleNamesArray objectAtIndex:indexPath.row]objectForKey:@"name"];
        NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[self.candidateRoleNamesArray valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
        
        if ([SocialCheck isEqualToString:@"Y"]) {
            
            [cell.profile_pic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[self.candidateRoleNamesArray valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
            
            
        }else{
            
            [cell.profile_pic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[self.candidateRoleNamesArray valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
            
        }
        
        if ([self.selectedArray containsObject:[[self.candidateRoleNamesArray objectAtIndex:  indexPath.row]objectForKey:@"userId"]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (searchEnabled) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.selectedArray addObject:[[searchResults objectAtIndex:indexPath.row]objectForKey:@"userId"]];
            NSLog(@"Search Select Array %@",self.selectedArray);
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.selectedArray removeObject:[[searchResults objectAtIndex:  indexPath.row]objectForKey:@"userId"]];
            NSLog(@"Search Select Array After Delete %@",self.selectedArray);
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }else{
    
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.selectedArray addObject:[[self.candidateRoleNamesArray objectAtIndex:indexPath.row]objectForKey:@"userId"]];
            NSLog(@"Select Array %@",self.selectedArray);
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.selectedArray removeObject:[[self.candidateRoleNamesArray objectAtIndex:  indexPath.row]objectForKey:@"userId"]];
            NSLog(@"Select Array After Delete %@",self.selectedArray);
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (IBAction)Done_btn:(id)sender {
    [self.delegate sendCandidateRolesToAddCandidate:self.selectedArray];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  UISearchDisplayController

//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
//{
//    NSLog(@"Scope %ld", (long)searchController.searchBar.selectedScopeButtonIndex);
//    // Set searchString equal to what's typed into the searchbar
//    NSString *searchString = self.searchBar.text;
//    
//    //self.searchController.searchBar.selectedScopeButtonIndex
//
//    [self updateFilteredContentForAirlineName:searchString scope: [NSString stringWithFormat: @"%ld", (long)self.searchBar.selectedScopeButtonIndex]];
//    
//}

- (void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    if (airlineName == nil) {
        // If empty the search results are the same as the original data
        searchResults = [self.candidateRoleNamesArray mutableCopy];
    } else {
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", airlineName];
            NSArray * search = [self.candidateRoleNamesArray filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.tableview reloadData];
        }else if ([scope isEqualToString:@"1"]){
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", airlineName];
            NSArray * search = [self.candidateRoleNamesArray filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
           searchResults = [NSMutableArray arrayWithArray:search];
            [self.tableview reloadData];
        }
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.tableview reloadData];
    }else {
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.search_Bar.selectedScopeButtonIndex]];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchEnabled = YES;
    [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.search_Bar.selectedScopeButtonIndex]];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.tableview reloadData];
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

@end
