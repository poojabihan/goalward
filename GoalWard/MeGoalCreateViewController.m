//
//  MeGoalCreateViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "MeGoalCreateViewController.h"
#import "GuidLineMeTableViewCell.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import <FCAlertView.h>
@interface MeGoalCreateViewController ()<NSURLSessionDataDelegate,NSURLSessionTaskDelegate,FCAlertViewDelegate>
{
    NSMutableArray *TitleData;NSInteger TagCount;UIDatePicker *datePicker;NSString *dateString;
    MBProgressHUD *hud;NSDate *eventDateStart;NSDate *eventDateEnd;
    NSString *stratDateString;
    NSString *endDateString;  UIColor *color;NSString *subtitleColor;
}
@property (retain, nonatomic) UIImage *alertImage;
@property (retain, nonatomic) NSString *alertTitle;
@property (retain, nonatomic) NSArray *arrayOfButtonTitles;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *AddTaskTextField;
@property NSDateFormatter *dateFormat;
@property NSDateFormatter *dateFormat2;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *AddGuideTextField;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *StartDateTextField;
@property (strong, nonatomic) IBOutlet UIButton *SubmitButtonInstance;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *EndDateTextField;
@end
@implementation MeGoalCreateViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormat2=[[NSDateFormatter alloc] init];
        [self.dateFormat2 setDateFormat:@"dd/MM/YYYY"];
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tableviewlayout.constant=0;
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    TitleData = [[NSMutableArray alloc] init];
    _ScrollContinView.backgroundColor=[UIColor clearColor];
    _tableview.scrollEnabled=YES;
    _tableview.backgroundColor=[UIColor clearColor];
    _tableview.estimatedRowHeight=_tableview.rowHeight;
    _tableview.rowHeight=UITableViewAutomaticDimension;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [self LoadingViewElementMeGoal];
}
-(void)LoadingViewElementMeGoal{
    
    _tableview.allowsSelection=YES;
    
    self.AddTaskTextField.backgroundColor=[UIColor clearColor];
    self.AddTaskTextField.layer.cornerRadius=2;
    self.AddTaskTextField.layer.borderWidth=1;
    self.AddTaskTextField.layer.borderColor=[UIColor whiteColor].CGColor;
    
    self.AddGuideTextField.backgroundColor=[UIColor clearColor];
    self.AddGuideTextField.layer.cornerRadius=2;
    self.AddGuideTextField.layer.borderWidth=1;
    self.AddGuideTextField.layer.borderColor=[UIColor whiteColor].CGColor;
    
    //[[UITextField appearanceWhenContainedIn:[UITextField class],nil] setTextColor:[UIColor whiteColor]];
    
    [self.SubmitButtonInstance setExclusiveTouch:YES];
    [self.SubmitButtonInstance.layer setMasksToBounds:YES];
    [self.SubmitButtonInstance.layer setCornerRadius:5.0f];
    self.SubmitButtonInstance.backgroundColor = [UIColor colorWithHex:@"3ac3f9"];
    
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showSelectedDate)];
    [doneBtn setTintColor:[UIColor colorWithHex:@"2E3992"]];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.StartDateTextField setInputAccessoryView:toolBar];
    [self.StartDateTextField setInputView:datePicker];
    [self.EndDateTextField setInputAccessoryView:toolBar];
    [self.EndDateTextField setInputView:datePicker];
    [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    datePicker.backgroundColor=[UIColor colorWithHex:@"2E3992"];
    self.title=@"Me Goal";
}
#pragma mark  DatePicker Methods
-(void) dateTextField:(id)sender
{
    if (TagCount == 3) {
        UIDatePicker *picker = (UIDatePicker*)self.StartDateTextField.inputView;
        eventDateStart = picker.date;
        dateString = [self.dateFormat stringFromDate:eventDateStart];
        [picker setMinimumDate:[NSDate date]];
        stratDateString = [self.dateFormat stringFromDate:eventDateStart];
        // self.StartDateTF.text = [NSString stringWithFormat:@"%@",dateString];
        NSString *datestring2=[self.dateFormat2 stringFromDate:eventDateStart];
        self.StartDateTextField.text=[NSString stringWithFormat:@"%@",datestring2];
    }else if (TagCount == 4){
        UIDatePicker *picker = (UIDatePicker*)self.EndDateTextField.inputView;
        [picker setMinimumDate:[self.dateFormat dateFromString:stratDateString]];
        eventDateEnd = picker.date;
       // NSString *dateString12 = [self.dateFormat stringFromDate:eventDateEnd];
        endDateString = [self.dateFormat stringFromDate:eventDateEnd];
        NSString *datestring2=[self.dateFormat2 stringFromDate:eventDateEnd];
        self.EndDateTextField.text = [NSString stringWithFormat:@"%@",datestring2];
    }
}
- (BOOL)textFieldShouldBeginEditing:(ACFloatingTextField *)textField {
    // Show UIPickerView
    TagCount =textField.tag;
    NSLog(@"Get Tag %li",(long)TagCount);
    return YES;
}
-(void)showSelectedDate{
        [self.StartDateTextField resignFirstResponder];
        [self.EndDateTextField resignFirstResponder];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification*)notification
{
   // NSDictionary *info = [notification userInfo];
 //   CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, 260, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
#pragma mark  UITextfield Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    TagCount =textField.tag;
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 3) {
        [textField resignFirstResponder];
    }else{
        [self jumpToNextTextField:textField withTag:nextTag];
    }
    return NO;
}
- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }else {
        [textField resignFirstResponder];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [TitleData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    GuidLineMeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GuidLineMeTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
   // _tableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"GuidLineMeTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.DeleteIcon.tag=indexPath.row;
    [cell.DeleteIcon addTarget:self action:@selector(DeleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.GuidLineText.text=[TitleData objectAtIndex:indexPath.row];
    return cell;
}
-(void)DeleteButtonClick:(UIButton*)sender{
    [TitleData removeObjectAtIndex:sender.tag];
    [_tableview reloadData];
}
- (IBAction)SubmitButtonAction:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSComparisonResult result;
        result = [eventDateStart compare:eventDateEnd];
        if(([self.AddTaskTextField.text isEqualToString:@""]) && ([self.AddGuideTextField.text isEqualToString:@""]) && ([self.StartDateTextField.text isEqualToString:@""]) && ([self.EndDateTextField.text isEqualToString:@""]))
        {
            [self validationErrorAlert:@"Please, Fill all the details."];
        }
        else if ([self.AddTaskTextField.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Add Your Task."];
        }else if ([self.AddGuideTextField.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Add Your Guide Line."];
        }else if ([self.StartDateTextField.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Select Start Date"];
        }else if ([self.EndDateTextField.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please, Select End Date"];
        }
        else if (result == NSOrderedDescending){
            [self validationErrorAlert:@"Please, Select End Date greater than Start Date"];
        }
        else
        {
//            NSString *joinedString = [TitleData componentsJoinedByString:@"-&@-"];
//            
//            NSLog(@"Guide Line List String %@ - ",joinedString);
            NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            NSLog(@"Store User Id %@",UserId);
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:TitleData options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"FriendList%@", jsonString);
            NSString *FinalString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSLog(@"Final String %@",FinalString);
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSDictionary *dictParam = @{@"userId":UserId,@"Task":self.AddTaskTextField.text,@"GuideLine":self.AddGuideTextField.text,@"StartDate":stratDateString,@"EndDate":endDateString};
            NSLog(@"Dic %@",dictParam);
            NSString *loginURL=[NSString stringWithFormat:@"%@addMeGoal",KBaseUrl];
          [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"response:%@",response);
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                        });
                        NSString *Goalie = [response valueForKey:@"goalie"];
                        NSString *SubTitle = [NSString stringWithFormat:@"%@ Goalies needed to complete your Goal.",Goalie];
                        self.alertImage = [UIImage imageNamed:@"GoliesIcon"];
                        [self showAlertInView:self withTitle:self.AddTaskTextField.text withSubtitle:SubTitle withCustomImage:self.alertImage withDoneButtonTitle:@"Done" andButtons:nil];
                    });
                }else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"Error" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
                else if (response==NULL) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    NSLog(@"response is null");
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
                
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
            [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
        }
}
- (IBAction)AddGuidLineButtonAction:(id)sender {
    if (_AddGuideTextField.text.length==0) {
           [self validationErrorAlert:@"Please, Fill the details."];
    }
    else{
        [TitleData addObject:_AddGuideTextField.text];
        self.AddGuideTextField.text=@"";
        [_tableview reloadData];
    }
}
#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
- (void) showAlertInView:(UIViewController *)view withTitle:(NSString *)title withSubtitle:(NSString *)subTitle withCustomImage:(UIImage *)image withDoneButtonTitle:(NSString *)done andButtons:(NSArray *)buttons {
    FCAlertView *alert = [[FCAlertView alloc] init];
    self.alertTitle = title;
    alert.subTitleColor = [self checkFlatColors:subtitleColor];
    alert.blurBackground = 1;
    alert.bounceAnimations = 1;
    alert.darkTheme = YES;
    alert.fullCircleCustomImage = YES;
    alert.delegate = self;
    //[alert makeAlertTypeSuccess];
    alert.avoidCustomImageTint = 1;
    alert.subtitleFont = [UIFont fontWithName:@"Avenir" size:17.0];
    self.arrayOfButtonTitles = @[];
    [alert showAlertInView:view withTitle:title withSubtitle:subTitle withCustomImage:image withDoneButtonTitle:done andButtons:buttons];
}
- (UIColor *) checkFlatColors:(NSString *)selectedColor {
    FCAlertView *alert = [[FCAlertView alloc] init];
    color = alert.flatTurquoise;
    return color;
}
-(void)FCAlertDoneButtonClicked:(FCAlertView *)alertView{
   [self.navigationController popViewControllerAnimated:YES];
}

@end
