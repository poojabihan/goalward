//
//  AllUserAndPendingRequestViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 06/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "AllUserAndPendingRequestViewController.h"
#import "AllUserPendingRequestTableViewCell.h"
#import "Webservice.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"
@interface AllUserAndPendingRequestViewController ()
{
    MBProgressHUD *HUD;NSString *IndexValue;NSString *loginURL;
    NSMutableArray *ListData;MBProgressHUD *AcceptHUD;NSString *AcceptFriendID;
    NSString *RejectFriendID;NSString *AccRejCheck;
}
@end
@implementation AllUserAndPendingRequestViewController
- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _SegmentBAckground.backgroundColor=[UIColor clearColor];
    self.Tableview.tableFooterView = [UIView new];

   // UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light-background-screen.png"]];
    //[tempImageView setFrame:_Tableview.frame];
   // _Tableview.backgroundView = tempImageView;
    self.Tableview.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    [_Tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    IndexValue=@"Request";
    [self LodingDataWebservices];
}
-(void)LodingDataWebservices{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        HUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        HUD.contentColor = [UIColor whiteColor];
        HUD.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam1 = @{@"userId":LoginUserId};
        if ([IndexValue isEqualToString:@"Request"]) {
            loginURL=[NSString stringWithFormat:@"%@showPendingList",KBaseUrl];
        }else{
           loginURL=[NSString stringWithFormat:@"%@showFriendList",KBaseUrl];
        }
        NSLog(@"Url Check - %@",loginURL);
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)){
                ListData = [response valueForKey:@"data"];
                NSLog(@"%@",ListData);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_Tableview reloadData];
                    //[_Tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [HUD hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    _Tableview.hidden=YES;
                }else{
                    _Tableview.hidden=NO;
                }
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SegmentActionTap:(id)sender {
    if(_SegmentOulet.selectedSegmentIndex==0){
        NSLog(@"First Segment Index ");
        IndexValue=@"Request";
        [self LodingDataWebservices];
    }else if (_SegmentOulet.selectedSegmentIndex==1){
        IndexValue=@"Friend";
        NSLog(@"Second Segment Index");
        [self LodingDataWebservices];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ListData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    AllUserPendingRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AllUserPendingRequestTableViewCell"];
    cell.ProfilePhoto.layer.cornerRadius = cell.ProfilePhoto.frame.size.width/2;
    cell.ProfilePhoto.layer.masksToBounds = YES;
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    //cell.BackView.layer.cornerRadius=10;
    //cell.BackView.layer.masksToBounds=YES;
    cell.AcceptBtnTap.layer.cornerRadius=5;
    cell.RejectBtn.layer.cornerRadius=5;
    cell.BackView.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    [cell.BackView.layer setCornerRadius:5.0f];
    [cell.BackView.layer setBorderColor:[UIColor clearColor].CGColor];
    [cell.BackView.layer setBorderWidth:0.2f];
    [cell.BackView.layer setShadowColor:[UIColor clearColor].CGColor];
    [cell.BackView.layer setShadowOpacity:5.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"AllUserPendingRequestTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if ([IndexValue isEqualToString:@"Friend"]) {
        cell.RejectBtn.hidden=YES;
        cell.AcceptBtnTap.hidden=YES;
    }else{
        cell.RejectBtn.hidden=NO;
        cell.AcceptBtnTap.hidden=NO;
    }
    cell.UserName.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"name"]objectAtIndex:indexPath.row]];
    cell.AcceptBtnTap.tag = indexPath.row;
    [cell.AcceptBtnTap addTarget:self action:@selector(AcceptButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.RejectBtn.tag = indexPath.row;
    [cell.RejectBtn addTarget:self action:@selector(RejectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
    
    if ([SocialCheck isEqualToString:@"Y"]) {
        
        [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
        
    }else{
        
        [cell.ProfilePhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }
    
    return cell;
}
-(void)AcceptButtonClick:(UIButton*)sender{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable)
    {
        AccRejCheck =@"Accept";
        AcceptFriendID = [[ListData valueForKey:@"userId"] objectAtIndex:sender.tag];
        [self AcceptAndRejectWebServices];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    NSLog(@"Accept");
}
-(void)RejectButtonClick:(UIButton*)sender{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable)
    {
        AccRejCheck=@"reject";
      AcceptFriendID =  [[ListData valueForKey:@"userId"] objectAtIndex:sender.tag];
    [self AcceptAndRejectWebServices];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    NSLog(@"Reject");
}
-(void)AcceptAndRejectWebServices{
    NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
    NSLog(@"Store User Id %@",LoginUserId);
    AcceptHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    AcceptHUD.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
    AcceptHUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    AcceptHUD.contentColor = [UIColor whiteColor];
   // AcceptHUD.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    NSDictionary *dictParam1 = @{@"userId":LoginUserId,@"friendId":AcceptFriendID};
    NSLog(@"Dic %@",dictParam1);
    if ([AccRejCheck isEqualToString:@"Accept"]) {
        loginURL=[NSString stringWithFormat:@"%@acceptFriendRequest",KBaseUrl];
    }else{
        loginURL=[NSString stringWithFormat:@"%@deleteFriendRequest",KBaseUrl];
    }
    NSLog(@"Url Check - %@",loginURL);
    [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
        NSLog(@"Services Response %@",response);
        if (([[response objectForKey:@"status"]boolValue] == 1)){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self LodingDataWebservices];
                [AcceptHUD hideAnimated:YES ];
                [_Tableview reloadData];
            });
        } else if (response==NULL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AcceptHUD hideAnimated:YES ];
            });
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            NSLog(@"response is null");
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [AcceptHUD hideAnimated:YES];
            });
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
        }
        
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AcceptHUD hideAnimated:YES ];
        });
        NSLog(@"Error %@",error);
    }];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //1. Define the initial state (Before the animation)
    cell.transform = CGAffineTransformMakeTranslation(0.f, 100);
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    //2. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.5];
    cell.transform = CGAffineTransformMakeTranslation(0.f, 0);
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
@end
