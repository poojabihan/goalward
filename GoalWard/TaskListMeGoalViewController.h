//
//  TaskListMeGoalViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 27/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskListMeGoalViewController : UIViewController


@property BOOL isPendingTask;

@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;


@property (strong, nonatomic) IBOutlet UITableView *TableView;
@end
