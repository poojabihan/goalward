//
//  ThirdTaskViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 01/12/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "ThirdTaskViewController.h"

@interface ThirdTaskViewController ()

@end

@implementation ThirdTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)InviteFriends:(id)sender {
    
    NSString *shareWithStringContent = @"Greetings Goal Oriented Friend! I’m using a goal setting app called GoalWard. If you connect with me in the app we can cheer each other on in our quest to complete our personal goals. Who isn’t in need of a fist bump every now and then? 🤜🏻🤛🏼  \n https://itunes.apple.com/us/app/goalward/id1333980275?mt=8&ign-mpt=uo%3D4";
    NSArray *itemsToShare = @[shareWithStringContent];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypePostToTencentWeibo];
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

@end
