//
//  CompliteTaskTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 03/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompliteTaskTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *TaskDetails;
@property (strong, nonatomic) IBOutlet UIButton *ShareButtonClick;

@property (strong, nonatomic) IBOutlet UILabel *TaskStartDate;
@property (strong, nonatomic) IBOutlet UILabel *TaskEndDate;
@property (strong, nonatomic) IBOutlet UIView *backview;
@property (strong, nonatomic) IBOutlet UILabel *erngolies;
@property (strong, nonatomic) IBOutlet UILabel *totalgoalies;
@property (strong, nonatomic) IBOutlet UIProgressView *ProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *progressLbl;
@property (strong, nonatomic) IBOutlet UIView *RatingView;

@end
