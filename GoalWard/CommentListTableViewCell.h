//
//  CommentListTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 24/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *UserProfile;
@property (strong, nonatomic) IBOutlet UILabel *UserComment;
@property (strong, nonatomic) IBOutlet UILabel *UserName;
@property (strong, nonatomic) IBOutlet UILabel *UserCommentDate;


@end
