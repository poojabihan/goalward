//
//  CommentViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "CommentViewController.h"
#import "Webservice.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "UIColor+Hexadecimal.h"
#import <FCAlertView.h>
#import "CommentHistoryViewController.h"
#import "UIImageView+WebCache.h"
#import "CommentGuidlineTableViewCell.h"
#import "TaskListViewController.h"
@interface CommentViewController ()<UITextFieldDelegate>{
    MBProgressHUD *hud;float RatingValue;NSMutableArray *GuideLine;
    NSMutableArray *GuidelineIds;  UIColor *color;NSString *subtitleColor;
    NSMutableArray *allUsersArr;
}
@property NSDateFormatter *dateFormat;
@property NSDateFormatter *dateFormat2;
@property NSMutableArray * selectedArray;
@property (retain, nonatomic) UIImage *alertImage;
@property (retain, nonatomic) NSString *alertTitle;
@property (strong, nonatomic) IBOutlet UILabel *GuidelineText;
@property (retain, nonatomic) NSArray *arrayOfButtonTitles;
@end

@implementation CommentViewController
- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self){
        self.dateFormat2 = [[NSDateFormatter alloc] init];
        [self.dateFormat2 setDateFormat:@"MM/dd/YYYY"];
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.TableView.backgroundColor=[UIColor clearColor];
    self.ContainView.backgroundColor=[UIColor clearColor];
    
    self.GuidelineText.layer.borderWidth=2;
    self.GuidelineText.layer.borderColor=[UIColor whiteColor].CGColor;
    self.GuidelineText.layer.cornerRadius=5;
    
    self.EarnGoliesView.backgroundColor=[UIColor clearColor];
    self.EarnGoliesView.layer.borderWidth=1;
    self.EarnGoliesView.layer.borderColor=[UIColor colorWithHex:@"12B7DD"].CGColor;
    self.EarnGoliesView.layer.masksToBounds=YES;
    self.TotalGoaliesView.backgroundColor=[UIColor clearColor];
    self.TotalGoaliesView.layer.borderWidth=1;
    self.TotalGoaliesView.layer.borderColor=[UIColor colorWithHex:@"12B7DD"].CGColor;
    self.TotalGoaliesView.layer.masksToBounds=YES;
    
    self.ShowAllCommentView.layer.cornerRadius=5;
    
    self.ShowTaskHistoryView.backgroundColor=[UIColor clearColor];
    self.ShowAllCommentView.backgroundColor=[UIColor clearColor];
    self.TbBackGroundView.backgroundColor=[UIColor clearColor];
    self.ProgressBar.layer.cornerRadius=5.0;
    self.ProgressBar.layer.masksToBounds=YES;
    GuideLine=[[NSMutableArray alloc] init];
    self.CommentTextField.layer.borderWidth=1.0;
    self.selectedArray = [[NSMutableArray alloc] init];
    self.CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [self TaskLoadDataWebServicesCall];
    
    allUsersArr = [[NSMutableArray alloc] init];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification*)notification{
     NSDictionary *info = [notification userInfo];
       CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height+50, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
-(void)keyboardWillBeHidden:(NSNotification*)notification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.CommentTextField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _CommentTextField.layer.borderColor=[UIColor clearColor].CGColor;
    return YES;
}
-(void)TaskLoadDataWebServicesCall{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        NSString  *CurrentDate =  [self.dateFormat2 stringFromDate:[NSDate date]];
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSDictionary *dictParam = @{@"eventId":self.EventID,@"date":CurrentDate};
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@showMyEvent",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response){
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                GuideLine = [[response valueForKey:@"Data"] valueForKey:@"guidelineids"];
                self.TaskTitle.text=[[response valueForKey:@"Data"] valueForKey:@"event"];
                
                NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
                
                self.TaskStartDate.text=[NSString stringWithFormat:@"Start Date : %@",[[response valueForKey:@"Data"] valueForKey:@"startDate"]];
                self.TaskEndDate.text=[NSString stringWithFormat:@"End Date :%@",[[response valueForKey:@"Data"] valueForKey:@"endDate"]];
                //guideline
                self.GuidelineText.text=[NSString stringWithFormat:@" %@",[[response valueForKey:@"Data"] valueForKey:@"guideline"]];
                self.EarnGoaliesLabel.text=[NSString stringWithFormat:@"%@",[[response valueForKey:@"Data"] valueForKey:@"earnGoalie"]];
                self.TotalGoaliesLabel.text=[NSString stringWithFormat:@"%@",[[response valueForKey:@"Data"] valueForKey:@"goalies"]];
                NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[response valueForKey:@"Data"] valueForKey:@"progress"]];
                
                allUsersArr = [[[response valueForKey:@"Data"] valueForKey:@"allUser"] mutableCopy];
                [allUsersArr removeObject:LoginUserId];
                
                NSLog(@"%@",progressValue);
                float Pro = [progressValue floatValue];
                self.ProgressBar.progress=Pro;
                GuidelineIds = [[[response valueForKey:@"Data"] valueForKey:@"guidelineids"]valueForKey:@"id"];
                for (int ch = 0; ch <[GuidelineIds count]; ch++) {
                    if ([[NSString stringWithFormat:@"%@",[[[[response valueForKey:@"Data"] valueForKey:@"guidelineids"]valueForKey:@"guidelineStatus"]objectAtIndex:ch]] isEqualToString:@"TRUE"]) {
                        NSString*GetID=[GuidelineIds objectAtIndex:ch];
                        [_selectedArray addObject: GetID];
                    }
                    NSLog(@"Select Array Web Services %@",_selectedArray);
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_TableView reloadData];
                    [hud hideAnimated:YES ];
                });
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [GuideLine count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    CommentGuidlineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentGuidlineTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"CommentGuidlineTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.GuidlienText.text=[NSString stringWithFormat:@"%@",[[GuideLine valueForKey:@"guideline"] objectAtIndex:indexPath.row]];
    
    if ([self.selectedArray containsObject:[GuidelineIds objectAtIndex:  indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        // cell.checkImage.image=[UIImage imageNamed:@"GuidlineCheck"];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        //cell.checkImage.image=[UIImage imageNamed:@"guidlineUncheck"];
    }
    
    return cell;
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
- (IBAction)SendComment:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        if (_CommentTextField.text.length==0) {
            _CommentTextField.layer.borderColor=[UIColor redColor].CGColor;
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSString *UserId=[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            NSDictionary *dictParam = @{@"eventId":_EventID,@"userId":UserId,@"comment":_CommentTextField.text,@"allUser":allUsersArr};
            NSLog(@"Dic %@",dictParam);
            NSString *loginURL=[NSString stringWithFormat:@"%@giveComment",KBaseUrl];
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"Response----- %@",response);
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _CommentTextField.text=@"";
                        [hud hideAnimated:YES ];
                    });
                }else{
                    NSLog(@"Failed");
                    [hud hideAnimated:YES ];
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Comment"]){
        CommentHistoryViewController * candRoleVC = [segue destinationViewController];
        candRoleVC.EventID=_EventID;
        candRoleVC.allUsersArr = allUsersArr;
    }if ([[segue identifier]isEqualToString:@"ShowTaskHistory"]) {
        TaskListViewController *VC = [segue destinationViewController];
        VC.EventID=_EventID;
    }
}
@end
