//
//  GuidLineMeTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuidLineMeTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *GuidLineText;

@property (strong, nonatomic) IBOutlet UIButton *DeleteIcon;



@end
