//
//  InviteFriendTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *BackView;
@property (strong, nonatomic) IBOutlet UILabel *TaskTitle;
@property (strong, nonatomic) IBOutlet UIImageView *UserProfile;
@property (strong, nonatomic) IBOutlet UILabel *UserName;
@property (strong, nonatomic) IBOutlet UILabel *TaskStratDate;
@property (strong, nonatomic) IBOutlet UILabel *TaskEndDate;
@property (strong, nonatomic) IBOutlet UIProgressView *ProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *ProgressLbl;
@property (strong, nonatomic) IBOutlet UILabel *EarnGoalies;
@property (strong, nonatomic) IBOutlet UILabel *TotalGoalies;
@property (strong, nonatomic) IBOutlet UIButton *SeeProfileButton;
@property (strong, nonatomic) IBOutlet UIView *ButtonBackgroundView;




@end
