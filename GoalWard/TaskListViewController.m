//
//  TaskListViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import "TaskListViewController.h"
#import "TaskHistoryTableViewCell.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
@interface TaskListViewController (){
    MBProgressHUD *hud;NSMutableArray *ListData;UIDatePicker *datePicker;NSDate *eventDateStart;NSString *SelectedDate;NSString *datestring2;
}
@property NSDateFormatter *dateFormat;
@end
@implementation TaskListViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormat=[[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.Tableview.backgroundColor=[UIColor clearColor];
    SelectedDate =  [self.dateFormat stringFromDate:[NSDate date]];
    _TaskName.backgroundColor=[UIColor clearColor];
    [self TaskHistoryWebservicesCall];
    self.Tableview.tableFooterView = [UIView new];

    
}

-(void)TaskHistoryWebservicesCall{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        NSLog(@"Today Date %@",SelectedDate);
        NSDictionary *dictParam1 = @{@"eventId":_EventID};
        NSLog(@"Dic %@",dictParam1);
        NSString *loginURL=[NSString stringWithFormat:@"%@getHistory",KBaseUrl];
         [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
             NSLog(@"response:%@",response);
             if (([[response objectForKey:@"status"]boolValue] == 1)) {
                 ListData = [response valueForKey:@"data"];
                 self.TaskName.text=[NSString stringWithFormat:@"%@",[ListData valueForKey:@"event"]];
                 self.TotalGolies.text=[NSString stringWithFormat:@"%@",[ListData valueForKey:@"earnGoalie"]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // [_TableView reloadData];
                     [_Tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                     [hud hideAnimated:YES];
                 });
                 if ([ListData count]==0) {
                     _Tableview.hidden=YES;
                 }else{
                     _Tableview.hidden=NO;
                 }
             }else if (response==NULL) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES ];
                 });
                 [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                 NSLog(@"response is null");
             }else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                 });
                 [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
             }
         } failure:^(NSError *error) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [hud hideAnimated:YES ];
             });
             NSLog(@"Error %@",error);
         }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[ListData valueForKey:@"history"] count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    TaskHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskHistoryTableViewCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"TaskHistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.GuidlineLabel.text=[NSString stringWithFormat:@"Date: %@",[[[ListData valueForKey:@"history"]valueForKey:@"date"]objectAtIndex:indexPath.row]];
    
    NSString*check=[NSString stringWithFormat:@"%@",[[[ListData valueForKey:@"history"] valueForKey:@"check"]objectAtIndex:indexPath.row]];
    
    if ([check isEqualToString:@"1"]) {
        cell.EarnGloiest.text=[NSString stringWithFormat:@"Earned Goalie: %@",[[[ListData valueForKey:@"history"]valueForKey:@"award"]objectAtIndex:indexPath.row]];
    }else{
        cell.EarnGloiest.text=[NSString stringWithFormat:@"Remove Goalie: %@",[[[ListData valueForKey:@"history"]valueForKey:@"award"]objectAtIndex:indexPath.row]];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
@end
