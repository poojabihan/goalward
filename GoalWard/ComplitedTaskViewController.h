//
//  ComplitedTaskViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 03/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplitedTaskViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;

@property (strong, nonatomic) IBOutlet UITableView *TableView;


@end
