//
//  GoalwardUserListTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 07/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalwardUserListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *BAckView;

@property (strong, nonatomic) IBOutlet UIImageView *ProfilePhoto;


@property (strong, nonatomic) IBOutlet UILabel *UserName;

@property (strong, nonatomic) IBOutlet UILabel *Rating;

@property (strong, nonatomic) IBOutlet UIButton *AddFriendsBtn;

@end
