//
//  ViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 10/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//
#import <EAIntroView/EAIntroView.h>
#import <SMPageControl/SMPageControl.h>
#import "ViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "TabBarViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "TabBarViewController.h"
#import "AppDelegate.h"

static NSString * const sampleDescription1 = @"";

@import FirebaseInstanceID;
@import Firebase;
@import FirebaseMessaging;
@interface ViewController ()<EAIntroDelegate>{
    MBProgressHUD * hud;
    NSString *fbid; NSString *fbprofile;NSString *fbLastname;NSString *fbFirstname;
    NSString *fbName;NSString *fbGender;NSString *fbEmail;
    NSUserDefaults *Store;NSString *FBProfileImage;UIView *rootView;
    EAIntroView *_intro;
}
@end
@implementation ViewController
@synthesize UserNameTF,PasswordTF;

- (void)viewDidLoad {
    
    [super viewDidLoad];
     rootView = self.navigationController.view;
     dispatch_async(dispatch_get_main_queue(),^{
         [self showIntroWithCrossDissolve];
     });
     Store=[NSUserDefaults standardUserDefaults];
     dispatch_async(dispatch_get_main_queue(), ^{
         [self LoadingViewElement];
     });
    
}

- (void)showIntroWithCrossDissolve {
    
    EAIntroPage *page1 = [EAIntroPage page];
    //page1.title = @"Hello world";
    //page1.desc = sampleDescription1;
    page1.bgImage = [UIImage imageNamed:@"intro1.jpg"];
    //page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title1"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    //page2.title = @"This is page 2";
    //page2.desc = sampleDescription2;
    page2.bgImage = [UIImage imageNamed:@"intro2.jpg"];
    //page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title2"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    //page3.title = @"This is page 3";
    //page3.desc = sampleDescription3;
    page3.bgImage = [UIImage imageNamed:@"intro3.jpg"];
    //page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title3"]];
    
    EAIntroPage *page4 = [EAIntroPage page];
    //page4.title = @"This is page 4";
    //page4.desc = sampleDescription4;
    page4.bgImage = [UIImage imageNamed:@"intro4.jpg"];
    //page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroPage *page5 = [EAIntroPage page];
    //page5.title = @"This is page 4";
    //page5.desc = sampleDescription4;
    page5.bgImage = [UIImage imageNamed:@"intro5.jpg"];
    //page5.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroPage *page6 = [EAIntroPage page];
    //page6.title = @"This is page 4";
    //page6.desc = sampleDescription4;
    page6.bgImage = [UIImage imageNamed:@"intro6.jpg"];
    //page6.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroPage *page7 = [EAIntroPage page];
    //page7.title = @"This is page 4";
    //page7.desc = sampleDescription4;
    page7.bgImage = [UIImage imageNamed:@"intro7.jpg"];
    //page7.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page1,page2,page3,page4,page5,page6,page7]];
    //intro.skipButtonAlignment = EAViewAlignmentCenter;
    //intro.skipButtonY = 80.f;
    [intro.skipButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    intro.pageControlY = 42.f;
    [intro setDelegate:self];
    [intro showInView:rootView animateDuration:0.3];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark  UITextfield Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    int duration = 1; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}

#pragma mark  Loading View

-(void)LoadingViewElement{
    
    [self.LoginButtonInstance setExclusiveTouch:YES];
    [self.LoginButtonInstance.layer setMasksToBounds:YES];
    [self.LoginButtonInstance.layer setCornerRadius:5.0f];
    //self.LoginButtonInstance.backgroundColor = [UIColor whiteColor];
  
    [self.SignUpButtonInstance setExclusiveTouch:YES];
    [self.SignUpButtonInstance.layer setMasksToBounds:YES];
    [self.SignUpButtonInstance.layer setCornerRadius:20.0f];
    self.SignUpButtonInstance.backgroundColor = [UIColor clearColor];
   
    [self.ForgotButtonInstance setExclusiveTouch:YES];
    [self.ForgotButtonInstance.layer setMasksToBounds:YES];
    [self.ForgotButtonInstance.layer setCornerRadius:20.0f];
    self.ForgotButtonInstance.backgroundColor = [UIColor clearColor];
    
    [self.LoginWithFbButtonInstance setExclusiveTouch:YES];
    self.LoginWithFbButtonInstance.backgroundColor = [UIColor clearColor];
}

#pragma mark  Submit Button

- (IBAction)Login_Submit_button:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if((([self.UserNameTF.text isEqualToString:@""])) && ([self.PasswordTF.text isEqualToString:@""])){
            
            [self validationErrorAlert:@"Please, Fill all the details."];
            
        }
        else if(![self NSStringIsValidEmail:self.UserNameTF.text]){
            
            [self validationErrorAlert:@"Invalid e-mail address!"];
            
        }else if ([self.PasswordTF.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Passwords do not match."];
            
        }else{
            
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.contentColor = khudColour;
            hud.label.text = NSLocalizedString(@"Signing...", @"HUD loading title");
            
            NSString *FCMToken = [[FIRInstanceID instanceID]token];
            NSDictionary *dictParam = @{@"email":self.UserNameTF.text,@"password":self.PasswordTF.text,@"device_token":FCMToken};
            
            NSString *loginURL=[NSString stringWithFormat:@"%@userLogin",KBaseUrl];
            
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                
                NSLog(@"response:%@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES afterDelay:2];
                        NSLog(@"Response Success");
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"record"]objectForKey:@"email"] forKey:@"email"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"record"]objectForKey:@"userId"] forKey:@"User_Id"];
                        
                        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
                        NSLog(@"Store User Id %@",UserId);
                        
                         [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"LoginTypeCheck"];
                        
                         [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"record"]objectForKey:@"name"] forKey:@"UserName"];
                        
                        NSString *imageUrl=[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[response valueForKey:@"record"]valueForKey:@"profileImage"]];
                        
                        NSLog(@"Image Url ; ; ; ; ; ; ; ; - %@",imageUrl);
                        
                        [[NSUserDefaults standardUserDefaults] setObject:imageUrl forKey:@"UserProfileImage"];
                        
                        [Store setObject:@"1" forKey:@"Login"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                        self.navigationController.navigationBarHidden=YES;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                            [self.navigationController pushViewController:obj animated:YES];
                        });
                        
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES ];
                    });
                    [self alertWithTitle:kAppNameAlert message:@"Invalid Email Id Or Passwords do not match" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Facebook butoon

- (IBAction)Fb_login_button:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
        [weakSelf fbLogin];
    
//     FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//     [login logInWithReadPermissions: @[@"public_profile",@"email"]
//     fromViewController:self
//     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
//     if (error){
//                NSLog(@"Process error");
//            }else if (result.isCancelled){
//                NSLog(@"Cancelled");
//            } else {
//     //"birthday"  will also be fethched
//
//    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
//    [parameters setValue:@"email,id,name,gender,first_name,last_name,picture" forKey:@"fields"];
//    if ([FBSDKAccessToken currentAccessToken])
//    {
//        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
//         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error){
//                            if (!error)
//                                        {
//                                            NSLog(@"fetched user:%@", result);
//
//                                            fbid          =     [result valueForKey:@"id"];
//                                            fbName        =     [result valueForKey:@"name"];
//
//                                            fbEmail       =     [result valueForKey:@"email"];
//                                            fbGender      =     [result valueForKey:@"gender"];
//                                            fbFirstname   =     [result valueForKey:@"first_name"];
//                                            fbLastname    =     [result     valueForKey:@"last_name"];
//                                            fbprofile     =     [result valueForKey:@"picture"];
//                                            FBProfileImage = [NSString stringWithFormat:@"%@",[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"]];
//                                            NSLog(@"Fb Profile Image Url ;- %@",FBProfileImage);
//                                            [self SocialLoginWebServices];
//                        }
//                    }
//                ];
//            }
//        }
//     }];
}


-(void)fbLogin{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];// adding this single line fixed my issue
    [login logInWithReadPermissions: @[@"public_profile"] fromViewController:self  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
            [login logOut];
            
        } else {
            NSLog(@"Logged in");
            [self GetData];
        }
    }]; // I called this logout function
}


- (void)GetData {
    if ([FBSDKAccessToken currentAccessToken]) {
        NSString * accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, picture.type(large) ,last_name"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 //NSDictionary *Result = result;
                 fbid          =     [result valueForKey:@"id"];
                fbName        =     [result valueForKey:@"name"];
                fbEmail       =     [result valueForKey:@"email"];
                fbGender      =     [result valueForKey:@"gender"];
                fbFirstname   =     [result valueForKey:@"first_name"];
                fbLastname    =     [result     valueForKey:@"last_name"];
                fbprofile     =     [result valueForKey:@"picture"];
                FBProfileImage = [NSString stringWithFormat:@"%@",[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"]];
                NSLog(@"Fb Profile Image Url ;- %@",FBProfileImage);
                [self SocialLoginWebServices];
                 NSDictionary *params = [NSMutableDictionary dictionaryWithObject:accessToken forKey:@"access_token"];
                 NSLog(@"Params %@ ",params);
                 
             } else {
                 NSLog(@"Error %@",[error description]);
             }
         }];
    }
    
}

-(void)SocialLoginWebServices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.contentColor = khudColour;
        hud.label.text = NSLocalizedString(@"Signing...", @"HUD loading title");
        
        NSString *FCMToken = [[FIRInstanceID instanceID]token];
   
        NSDictionary *dictParam = @{@"name":fbName,@"imageURL":FBProfileImage,@"socialId":fbid,@"device_token":FCMToken};
        
        NSLog(@"dic %@",dictParam);
        
        NSString *loginURL=[NSString stringWithFormat:@"%@socialLogin",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
            NSLog(@"%@",response);
            
            NSString *LoginType = [[response objectForKey:@"result"] objectForKey:@"socialStatus"];
            
            if ([LoginType isEqualToString:@"Y"]) {
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"LoginTypeCheck"];
            };
            
            [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"email"] forKey:@"email"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"userid"] forKey:@"User_Id"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"result"]objectForKey:@"name"] forKey:@"UserName"];
            
            NSString *imageUrl=[NSString stringWithFormat:@"%@",[[response valueForKey:@"result"]valueForKey:@"imageURL"]];
            
            NSLog(@"Image Url ; ; ; ; ; ; ; ; - %@",imageUrl);
            
            [[NSUserDefaults standardUserDefaults] setObject:imageUrl forKey:@"imageURL"];
            
            [Store setObject:@"1" forKey:@"Login"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            
            self.navigationController.navigationBarHidden=YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                [self.navigationController pushViewController:obj animated:YES];
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"--------------------------Memory--Warning-------------------");
}
@end
