//
//  GuideLineRatingTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 02/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideLineRatingTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *guideTesx;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;


@end
