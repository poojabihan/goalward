//
//  SettingTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 31/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *ItemName;
@property (strong, nonatomic) IBOutlet UIImageView *item_Image;

@end
