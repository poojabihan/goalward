//
//  AllUserAndPendingRequestViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 06/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllUserAndPendingRequestViewController : UIViewController



@property (strong, nonatomic) IBOutlet UITableView *Tableview;
@property (strong, nonatomic) IBOutlet UIView *SegmentBAckground;
@property (strong, nonatomic) IBOutlet UISegmentedControl *SegmentOulet;

- (IBAction)SegmentActionTap:(id)sender;



@end
