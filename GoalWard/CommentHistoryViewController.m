//
//  CommentHistoryViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 23/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "CommentHistoryViewController.h"
#import "CommentListTableViewCell.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"
@interface CommentHistoryViewController ()
{
    NSArray*TitleData;MBProgressHUD *hud;NSMutableArray *ListData;
}
@end

@implementation CommentHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.txtComment.layer.borderWidth=1.0;
    self.txtComment.layer.borderColor=[UIColor clearColor].CGColor;
    self.TableView.estimatedRowHeight=_TableView.rowHeight;
    self.TableView.rowHeight=UITableViewAutomaticDimension;
    NSLog(@"----------------------------------%@",self.EventID);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.TableView.tableFooterView = [UIView new];

    [self CommentLoadingServices];
    
    if (_isFromNotifications) {
        _allUsersArr = [[NSMutableArray alloc] init];
        [self getEventDetail];
    }
    
}

- (IBAction)btnCommentAction:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable){
        if (_txtComment.text.length==0) {
            _txtComment.layer.borderColor=[UIColor redColor].CGColor;
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.contentColor = [UIColor whiteColor];
            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            NSString *UserId=[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
            NSDictionary *dictParam = @{@"eventId":_EventID,@"userId":UserId,@"comment":_txtComment.text,@"allUser":_allUsersArr};
            NSLog(@"Dic %@",dictParam);
            NSString *loginURL=[NSString stringWithFormat:@"%@giveComment",KBaseUrl];
            [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"Response----- %@",response);
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                   
           dispatch_async(dispatch_get_main_queue(), ^{
               
               [self CommentLoadingServices];
                        _txtComment.text=@"";
                        [hud hideAnimated:YES ];
                    });
                }else{
                    NSLog(@"Failed");
                    [hud hideAnimated:YES ];
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }

    
}

-(void)getEventDetail{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSDateFormatter *Dateformate2 = [[NSDateFormatter alloc] init];
        [Dateformate2 setDateFormat:@"MM/dd/YYYY"];
        NSString  *CurrentDate =  [Dateformate2 stringFromDate:[NSDate date]];
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSLog(@"Task Id %@  -  current Date %@",_EventID,CurrentDate);
        NSDictionary *dictParam = @{@"eventId":_EventID,@"date":CurrentDate};
        NSLog(@"Dic %@",dictParam);
        NSString *loginURL=[NSString stringWithFormat:@"%@showMyEvent",KBaseUrl];
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *LoginUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
                    
                    ListData = [response valueForKey:@"Data"];
                    _allUsersArr = [[[response valueForKey:@"Data"] valueForKey:@"allUser"] mutableCopy];
                    [_allUsersArr removeObject:LoginUserId];
                    
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [self.TableView reloadData];
                    [hud hideAnimated:YES];
                });
                NSLog(@"Response Success");
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  UITextfield Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.txtComment.layer.borderColor=[UIColor clearColor].CGColor;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.txtComment resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _txtComment.layer.borderColor=[UIColor clearColor].CGColor;
    return YES;
}

-(void)CommentLoadingServices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
//        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
//        hud.contentColor = [UIColor whiteColor];
//        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        
        NSDictionary *dictParam1 = @{@"eventId":self.EventID};
        
        NSLog(@"Dic %@",dictParam1);
        
        NSString *loginURL=[NSString stringWithFormat:@"%@showComments",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [hud hideAnimated:YES ];
//            });
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ListData = [response valueForKey:@"comments"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_TableView reloadData];
//                    [hud hideAnimated:YES ];
                });
                
                if ([ListData count]==0) {
                    
                    self.TableView.hidden=YES;
                }
                else
                {
                    self.TableView.hidden=NO;
                    
                }
            }
            else if (response==NULL) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [hud hideAnimated:YES ];
//                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                    NSLog(@"response is null");
                }else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [hud hideAnimated:YES];
//                    });
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                }
            } failure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [hud hideAnimated:YES ];
//                });
                NSLog(@"Error %@",error);
            }];
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ListData count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    CommentListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentListTableViewCell"];
    cell.UserProfile.layer.cornerRadius = cell.UserProfile.frame.size.width/2;
    cell.UserProfile.layer.masksToBounds = YES;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"CommentListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
    
    if ([SocialCheck isEqualToString:@"Y"]) {
        
        [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
        
    }else{
        
        [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }
    cell.UserComment.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"comment"]objectAtIndex:indexPath.row]];
    cell.UserName.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"name"]objectAtIndex:indexPath.row]];
    cell.UserCommentDate.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"date"]objectAtIndex:indexPath.row]];
    return cell;
}

-(void)refreshView:(NSString *)eventID {
    self.EventID = eventID;
    [self CommentLoadingServices];
}

@end
