//
//  SettingViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 13/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "SettingViewController.h"
#import "ViewController.h"
#import "UIColor+Hexadecimal.h"
#import "SettingTableViewCell.h"
#import "TaskListMeGoalViewController.h"
#import "WeGoalPeopleViewController.h"
#import "ProfileUpdateViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "ChangePasswordViewController.h"
#import <AFHTTPSessionManager.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImageView+WebCache.h"
#import "HowToUsedGoalwardViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface SettingViewController ()<MFMailComposeViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSUserDefaults *Store;
    NSArray *item;NSArray*images;NSString *imgString;
    NSArray *userData;UIImage *chosenImage;
}
@end
@implementation SettingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tablrview.tableFooterView = [UIView new];

     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // self.tabBarItem.badgeValue = @"5";
    item = [NSArray arrayWithObjects:@"Change Password",@"Notification",@"Invite Friend",@"Email US",@"How To Use GoalWard?",@"Logout", nil];
    images = [NSArray arrayWithObjects:@"ProfileChangePassword",@"ProfileNotification",@"ProfileInviteFriends",@"EmailUS",@"ContactUS",@"Logout",nil];
     Store=[NSUserDefaults standardUserDefaults];
    _ProfileBackView.backgroundColor=[UIColor clearColor];
    _ProfileImage.layer.cornerRadius = _ProfileImage.frame.size.width/2;
    _ProfileImage.layer.masksToBounds = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.tablrview.scrollEnabled=NO;
    [self.tablrview setBackgroundColor:[UIColor whiteColor]];
    userData = [[NSArray alloc] init];
    NSString *ProfileImage =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserProfileImage"];
    NSString *UserName =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserName"];
    _UserName.text=UserName;
    
    NSLog(@"LoginValueCheck %@",[Store valueForKey:@"LoginTypeCheck"]);

    if ([[Store valueForKey:@"LoginTypeCheck"] isEqualToString:@"Yes"]) {
        //imageURL
        NSString *fbprofile =[[NSUserDefaults standardUserDefaults]valueForKey:@"imageURL"];
        self.ProfileUpdateButton.hidden=YES;
        [self.ProfileImage setImageWithURL:[NSURL URLWithString:fbprofile]
                          placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
    }else{
        self.ProfileUpdateButton.hidden=NO;
        [self.ProfileImage setImageWithURL:[NSURL URLWithString:ProfileImage]
                   placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
    }
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.tablrview reloadData];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [item count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"SettingTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.ItemName.text=[item objectAtIndex:indexPath.row];
    cell.item_Image.image=[UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Selected %ld",(long)indexPath.row);
    
    //UIViewController *otherViewCon;
    // like Main.storyboard for used "Main"
    
    switch (indexPath.row)
    {
        case 0:
        [self TaskListClick];
        break;
        case 1:
        [self ProfileUpdateClick];
        break;
        case 2:
        [self invitefriends];
        break;
        case 3:
        [self emailUSClick];
        break;
        case 4:
        [self visitWebsiteClick];
        break;
        case 5:
        [self LogoutClick];
        break;
    }
}

-(void)invitefriends{
    NSString *shareWithStringContent = @"Greetings Goal Oriented Friend! I’m using a goal setting app called GoalWard. If you connect with me in the app we can cheer each other on in our quest to complete our personal goals. Who isn’t in need of a fist bump every now and then? 🤜🏻🤛🏼  \n https://itunes.apple.com/us/app/goalward/id1333980275?mt=8&ign-mpt=uo%3D4";
    NSArray *itemsToShare = @[shareWithStringContent];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypePostToTencentWeibo];
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)ProfileUpdateClick{
    ProfileUpdateViewController *navi = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileUpdateViewController"];
    [self.navigationController pushViewController:navi animated:YES];
}

-(void)FriendListClick{
    WeGoalPeopleViewController *navi = [self.storyboard instantiateViewControllerWithIdentifier:@"WeGoalPeopleViewController"];
    [self.navigationController pushViewController:navi animated:YES];
}

#pragma mark  Email Send

-(void)emailUSClick{
    
    NSString *emailTitle = @"Goalward";
    // Email Content
    NSString *messageBody = @"Hello, Goalward";
    // To address
    NSArray * toRecipents = [NSArray arrayWithObject:@"goalwardapp@gmail.com"];
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:nil];
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  VisitWebsite

-(void)visitWebsiteClick{
    HowToUsedGoalwardViewController *SecVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HowToUsedGoalwardViewController"];
    [self.navigationController pushViewController:SecVC animated:YES];
   //[[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://chetaru.co.uk/"] options:@{} completionHandler:nil];
}

#pragma mark  Support

-(void)supportClick{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@",@"441325734845"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
    } else {
        [self alertWithTitle:kAppNameAlert message:@"Call facility is not available!!!" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)LogoutClick{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert    message:@"Sure want to Logout?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * logout = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSDictionary *dictParam = @{@"userId":UserId};
        NSString *loginURL=[NSString stringWithFormat:@"%@logout",KBaseUrl];
        NSLog(@"Url %@",loginURL);
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response){
            if (([[response objectForKey:@"status"]boolValue] == 1)){
                [[FBSDKLoginManager new] logOut];
                [Store setObject:@"0" forKey:@"Login"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profileImage"];
                [Store synchronize];
                ViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                VC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:VC animated:NO];
            }
        }failure:^(NSError *error){
            NSLog(@"Error %@",error);
        }];
    }];
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:logout];
    [alert addAction:dismiss];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

-(void)TaskListClick{
    ChangePasswordViewController *navi = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    [self.navigationController pushViewController:navi animated:YES];
}

#pragma mark  AlertPrompt

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

- (IBAction)Logout_btn:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert    message:@"Sure want to Logout?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * logout = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSDictionary *dictParam = @{@"userId":UserId};
        NSString *loginURL=[NSString stringWithFormat:@"%@logout",KBaseUrl];
        NSLog(@"Url %@",loginURL);
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
           if (([[response objectForKey:@"status"]boolValue] == 1)) {
               NSLog(@"Logout Response %@",response);
               [Store setObject:@"0" forKey:@"Login"];
               [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profileImage"];
               [Store synchronize];
               ViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
               VC.hidesBottomBarWhenPushed = YES;
               [self.navigationController pushViewController:VC animated:NO];
           }
        } failure:^(NSError *error) {
            NSLog(@"Error %@",error);
        }];
        
    }];
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:logout];
    [alert addAction:dismiss];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
    
}



- (IBAction)UpdateProfile:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
        }
        else
        {
            //[self showingAlert:@"Camera Not Found" :@"This Device has no Camera"];
            
            [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"Dismiss"];
        }
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
            photoPicker.delegate = self;
            photoPicker.allowsEditing = NO;
            photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            //  [self.navigationController.tabBarController presentViewController:photoPicker animated:YES completion:nil];
            [self presentViewController:photoPicker animated:YES completion:NULL];
        }
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)showcamera
{
    
    [self.tabBarController.tabBar setHidden:YES];
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];
    }
    else
    {
        [self camDenied];
    }
    
}

-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.tabBarController.tabBar setHidden:NO];
    
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    self.ProfileImage.image = chosenImage;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.ProfileImage.image, nil, nil, nil);
    }
    
//    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
//    // define the block to call when we get the asset based on the url (below)
//    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset){
//        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
//        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
//    };
//    // get the asset library and fetch the asset based on the ref url (pass in block above)
//    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
//    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
//
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
        [self ProfileUploadingWebservices];
    });
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
-(void)ProfileUploadingWebservices{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if([self image:self.ProfileImage.image isEqualTo:[UIImage imageNamed:@"blank_icon.png"]]==YES){
            imgString=@"";
        }else{
            NSData *imageData = UIImageJPEGRepresentation(self.ProfileImage.image, 0.2);
            imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            NSString *imgName = [_ProfileImage image].accessibilityIdentifier;
            NSLog(@"%@",imgName);
           
        }
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSLog(@"Store User Id %@",UserId);
        
        MBProgressHUD  *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        HUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        HUD.contentColor = [UIColor whiteColor];
        HUD.label.text = NSLocalizedString(@"Uploading...", @"HUD loading title");
        
        NSDictionary *dictParam = @{@"userId":UserId,@"profileImage":imgString};
        
        NSLog(@"Dic Param %@",dictParam);
        
        
        NSString *loginURL=[NSString stringWithFormat:@"%@updateProfile",KBaseUrl];
        
        NSLog(@"Url %@",loginURL);
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"Get response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
                NSString *imageUrl=[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[response valueForKey:@"result"]valueForKey:@"profileImage"]];
                NSLog(@"Image Url ; ; ; ; ; ; ; ; - %@",imageUrl);
                [[NSUserDefaults standardUserDefaults] setObject:imageUrl forKey:@"UserProfileImage"];
//
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [_ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[response valueForKey:@"result"]valueForKey:@"profileImage"]]]
                       //           placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
                    [self removeAllStoredCredentials];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideAnimated:YES];
                    });
                    NSLog(@"Response Success");
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Profile Update Success." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert addAction:ok];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:true completion:nil];
                    });
                });
            }else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES afterDelay:1];
                });
                [self alertWithTitle:kAppNameAlert message:@"User name or password is incorrect" actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES afterDelay:1];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideAnimated:YES afterDelay:1];
                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    if ([data1 isEqualToData:data2]) {
        return YES;
    }else{
        return NO;
    }
}
- (void)removeAllStoredCredentials
{
    // Delete any cached URLrequests!
    NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    [sharedCache removeAllCachedResponses];
    
    // Also delete all stored cookies!
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookies];
    id cookie;
    for (cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
    
    NSDictionary *credentialsDict = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];
    if ([credentialsDict count] > 0) {
        // the credentialsDict has NSURLProtectionSpace objs as keys and dicts of userName => NSURLCredential
        NSEnumerator *protectionSpaceEnumerator = [credentialsDict keyEnumerator];
        id urlProtectionSpace;
        // iterate over all NSURLProtectionSpaces
        while (urlProtectionSpace = [protectionSpaceEnumerator nextObject]) {
            NSEnumerator *userNameEnumerator = [[credentialsDict objectForKey:urlProtectionSpace] keyEnumerator];
            id userName;
            // iterate over all usernames for this protectionspace, which are the keys for the actual NSURLCredentials
            while (userName = [userNameEnumerator nextObject]) {
                NSURLCredential *cred = [[credentialsDict objectForKey:urlProtectionSpace] objectForKey:userName];
                NSLog(@"credentials to be removed: %@", cred);
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:cred forProtectionSpace:urlProtectionSpace];
            }
        }
    }
}
@end
