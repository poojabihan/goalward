//
//  InviteFriendsListViewController.m
//  GoalWard
//
//  Created by Alok Mishra on 22/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "InviteFriendsListViewController.h"
#import "InviteFriendTableViewCell.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import <MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "CommentViewController.h"
@interface InviteFriendsListViewController ()
{
    MBProgressHUD *hud;NSMutableArray *ListData;NSArray *searchResults;
    BOOL searchEnabled;
    
}
@property NSDateFormatter *dateFormat;
@end

@implementation InviteFriendsListViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormat = [[NSDateFormatter alloc] init];
        [self.dateFormat setDateFormat:@"MM/dd/YYYY - HH:mm:ss"];
    }
    return self;
}
- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.TableView.backgroundColor=[UIColor colorWithHex:@"0b122f"];
    [self.SearchBar setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    [self.SearchBar setBarTintColor:[UIColor colorWithHex:@"0b122f"]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithHex:@"0b122f"]];
    self.TableView.estimatedRowHeight=_TableView.rowHeight;
    self.TableView.rowHeight=UITableViewAutomaticDimension;
    self.TableView.tableFooterView = [UIView new];

    [self LoadingInvitefrienfList];

}
-(void)LoadingInvitefrienfList{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.contentColor = [UIColor whiteColor];
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"User_Id"];
        NSString  *CurrentDate =  [self.dateFormat stringFromDate:[NSDate date]];
        NSLog(@"Store User Id %@",UserId);
        
        NSDictionary *dictParam1 = @{@"userId":UserId,@"currentDateTime":CurrentDate};
        
        NSLog(@"Dic %@",dictParam1);
        
        NSString *loginURL=[NSString stringWithFormat:@"%@inviteUserEventList",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam1 success:^(NSDictionary *response) {
            
            NSLog(@"response:%@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ListData = [response valueForKey:@"Data"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                     [_TableView reloadData];
                    [hud hideAnimated:YES];
                });
                if ([ListData count]==0) {
                    _TableView.hidden=YES;
                }else{
                    _TableView.hidden=NO;
                }
            }
            else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES ];
                });
                [self alertWithTitle:kAppNameAlert message:@"Record Not Found." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                 [self alertWithTitle:kAppNameAlert message:@"Record Not Found." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES ];
            });
            NSLog(@"Error %@",error);
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}
-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        return [searchResults count];
    }
    else{
        return [ListData count];
    }
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    InviteFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InviteFriendTableViewCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.BackView.backgroundColor=[UIColor clearColor];
    [cell.BackView.layer setCornerRadius:5.0f];
    [cell.BackView.layer setBorderColor:[UIColor clearColor].CGColor];
    [cell.BackView.layer setBorderWidth:0.2f];
    [cell.BackView.layer setShadowColor:[UIColor clearColor].CGColor];
    [cell.BackView.layer setShadowOpacity:5.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    cell.UserProfile.layer.cornerRadius = cell.UserProfile.frame.size.width/2;
    cell.UserProfile.layer.masksToBounds = YES;
    cell.ProgressBar.layer.cornerRadius=5.0;
    cell.ProgressBar.layer.masksToBounds=YES;
    cell.ButtonBackgroundView.backgroundColor=[UIColor clearColor];
    cell.SeeProfileButton.layer.cornerRadius=10;
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"InviteFriendTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
     if (searchEnabled) {
         
         
    cell.UserName.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"name"]objectAtIndex:indexPath.row]];
         
    cell.TaskTitle.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"event"]objectAtIndex:indexPath.row]];
         
    cell.TaskStratDate.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
         
    cell.TaskEndDate.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"endDate"]objectAtIndex:indexPath.row]];
         
    cell.ProgressLbl.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"progress"]objectAtIndex:indexPath.row]];
         
    cell.EarnGoalies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
         
    cell.TotalGoalies.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
         
    NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[searchResults  valueForKey:@"progress"]objectAtIndex:indexPath.row]];
         
    NSLog(@"%@",progressValue);
    float Pro = [progressValue floatValue];
    cell.ProgressBar.progress=Pro;
         
    NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
         
    if ([SocialCheck isEqualToString:@"Y"]) {
       
    [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
        
    }else{
        
      [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goalwardapp.com/goalwardapp/public/uploads/%@",[[searchResults valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
        
    }
       
    cell.SeeProfileButton.tag = indexPath.row;
    [cell.SeeProfileButton addTarget:self action:@selector(SeeProfileDetails:) forControlEvents:UIControlEventTouchUpInside];
     
         
     }else{
         
         
         cell.UserName.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"name"]objectAtIndex:indexPath.row]];
         
         cell.TaskTitle.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"event"]objectAtIndex:indexPath.row]];
         
         cell.TaskStratDate.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"startDate"]objectAtIndex:indexPath.row]];
         
         cell.TaskEndDate.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"endDate"]objectAtIndex:indexPath.row]];
         
         cell.ProgressLbl.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"progress"]objectAtIndex:indexPath.row]];
         
         cell.EarnGoalies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"earnGoalie"]objectAtIndex:indexPath.row]];
         
         cell.TotalGoalies.text=[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"goalie"]objectAtIndex:indexPath.row]];
         
         NSString*progressValue=[NSString stringWithFormat:@"0.%@",[[ListData  valueForKey:@"progress"]objectAtIndex:indexPath.row]];
         
         NSLog(@"%@",progressValue);
         float Pro = [progressValue floatValue];
         cell.ProgressBar.progress=Pro;
         
         
         NSString *SocialCheck = [NSString stringWithFormat:@"%@",[[ListData valueForKey:@"socialStatus"]objectAtIndex:indexPath.row]];
         
         if ([SocialCheck isEqualToString:@"Y"]) {
             
             [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[ListData valueForKey:@"imageURL"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
             
         }else{
             
             [cell.UserProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://chetaru.gottadesigner.com/goalward/public/uploads/%@",[[ListData valueForKey:@"profileImage"]  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"blank_icon.png"]];
             
         }
         
         cell.SeeProfileButton.tag = indexPath.row;
         [cell.SeeProfileButton addTarget:self action:@selector(SeeProfileDetails:) forControlEvents:UIControlEventTouchUpInside];
     
     }
    
    return cell;
}
-(void)SeeProfileDetails:(UIButton*)sender
{
    if (searchEnabled) {
        CommentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
        vc.EventID=[NSString stringWithFormat:@"%@",[[searchResults  valueForKey:@"eventId"]objectAtIndex:sender.tag]];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        CommentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
        vc.EventID=[NSString stringWithFormat:@"%@",[[ListData  valueForKey:@"eventId"]objectAtIndex:sender.tag]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
- (void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    
    if (airlineName == nil) {
        
        // If empty the search results are the same as the original data
        searchResults = [ListData mutableCopy];
        
    } else {
        
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
            
        }
        else if ([scope isEqualToString:@"1"]){
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(event contains[cd] %@)", airlineName];
            NSArray * search = [ListData filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
            
        }
    }
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.TableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
        
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length==0) {
        [searchBar resignFirstResponder];
    }else{
        [searchBar resignFirstResponder];
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.TableView reloadData];
    
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    return YES;
}

@end
