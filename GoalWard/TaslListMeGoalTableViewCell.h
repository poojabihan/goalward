//
//  TaslListMeGoalTableViewCell.h
//  GoalWard
//
//  Created by Alok Mishra on 27/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaslListMeGoalTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *Task_Start_Date;
@property (strong, nonatomic) IBOutlet UILabel *Task_End_Date;
@property (strong, nonatomic) IBOutlet UILabel *User_Task;
@property (strong, nonatomic) IBOutlet UIView *backview;
@property (strong, nonatomic) IBOutlet UILabel *TotalGoalies;
@property (strong, nonatomic) IBOutlet UILabel *ErnGolies;

@end
