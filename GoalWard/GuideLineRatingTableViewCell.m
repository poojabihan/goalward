//
//  GuideLineRatingTableViewCell.m
//  GoalWard
//
//  Created by Alok Mishra on 02/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "GuideLineRatingTableViewCell.h"

@implementation GuideLineRatingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
