//
//  InviteFriendsListViewController.h
//  GoalWard
//
//  Created by Alok Mishra on 22/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsListViewController : UIViewController



@property (strong, nonatomic) IBOutlet UITableView *TableView;

@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;





@end
